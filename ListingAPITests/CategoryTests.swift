//
//  CategoryTests.swift
//  ListingAPITests
//
//  Created by Max on 17/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import XCTest
@testable import ListingAPI

class CategoryTests: XCTestCase {

    func testInvalidData() {
        let missingName = """
             {
                  "Number": "0153-0435-",
                  "IsLeaf": false
                }
        """
        let missingNumber =  """
             {
                "Name": "Boys",
                  "IsLeaf": false
                }
        """
        XCTAssertThrowsError(try ListingAPIDecoding.defaultDecoder.decode(ListingAPI.Category.self, from: missingName.utf8Data))
        XCTAssertThrowsError(try ListingAPIDecoding.defaultDecoder.decode(ListingAPI.Category.self, from: missingNumber.utf8Data))
    }

    func testSingleCategory() {
        let singleCateogory = """
             {
                  "Name": "Boys",
                  "Number": "0153-0435-",
                  "IsLeaf": false
                }
        """
        let output: ListingAPI.Category = DeserializationTestHelpers.deseralizeNoThrow(json: singleCateogory)
        XCTAssertEqual(output, ListingAPI.Category(name: "Boys", number: "0153-0435-", subcategories: nil, isLeaf: false))
    }

    func testCategoryWithSubcategories() {
        let categoryWithSubcategories = """
            {
              "Name": "Clothing & Fashion",
              "Number": "0153-",
              "Subcategories": [
                {
                  "Name": "Boys",
                  "Number": "0153-0435-",
                  "IsLeaf": false
                },
                {
                  "Name": "Girls",
                  "Number": "0153-0436-",
                  "IsLeaf": false
                }
              ],
              "IsLeaf": false
            }

        """
        let output: ListingAPI.Category = DeserializationTestHelpers.deseralizeNoThrow(json: categoryWithSubcategories)
        XCTAssertEqual(output, ListingAPI.Category(name: "Clothing & Fashion",
                                                   number: "0153-",
                                                   subcategories: [ListingAPI.Category(name: "Boys", number: "0153-0435-", subcategories: nil, isLeaf: false),
                                                                   ListingAPI.Category(name: "Girls", number: "0153-0436-", subcategories: nil, isLeaf: false)],
                                                   isLeaf: false))
    }
    
}

enum DeserializationTestHelpers {

    static func deseralizeNoThrow<T: Decodable>(json: String) -> T {
        do {
            return try deserialize(json: json)
        } catch {
            XCTFail("Couldn't deserialize with \(error)")
            fatalError(error.localizedDescription) // XCTFail doesn't return Never :(
        }
    }

    static func deserialize<T: Decodable>(json: String, decoder: JSONDecoder = ListingAPIDecoding.defaultDecoder) throws -> T {
        return try decoder.decode(T.self, from: json.utf8Data)
    }
}
