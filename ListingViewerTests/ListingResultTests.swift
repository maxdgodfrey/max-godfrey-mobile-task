//
//  ListingResultTests.swift
//  ListingViewerTests
//
//  Created by Max on 20/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import XCTest
import PromiseKit
@testable import ListingViewer
@testable import ListingAPI

final class StubbedMockBroker: ListingBrokerProtocol {

    func rootCategory() -> Promise<ListingAPI.Category> {
         fatalError("Not implemented")
    }

    func categoryWith(identifier: String) -> Promise<ListingAPI.Category> {
        fatalError("Not implemented")
    }

    func listingsForCategory(_ categoryId: String) -> Promise<SearchResult> {
        fatalError("Not implemented")
    }

    func listingResultImage(for url: URL) -> Promise<UIImage> {
        return Promise(error: TMError.unknown)
    }

    func listingDetailFor(identifier: Int) -> Promise<ListingDetail> {
        fatalError("Not implemented")
    }
}

class ListingResultTests: XCTestCase {

    let mockBroker = StubbedMockBroker()

    func testModelToViewState() {
        let models: [Listing] = [Listing(identifier: 11111,
                              title: "This is a title",
                              subtitle: "This is a subtitle",
                              priceDisplay: "$12.00",
                              imageHref: nil)]
        // Only thing to test currently is that the identifier gets converted correctly.
        if case let ListingResultViewState.Result.data(listings) = ListingResultPresenter.stateFrom(models: models, broker: mockBroker) {
            XCTAssertEqual(11111, listings.first?.identifier ?? 0)
        } else {
            XCTFail()
        }
    }
}
