//
//  ListingViewerTests.swift
//  ListingViewerTests
//
//  Created by Max on 16/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import XCTest
import PromiseKit
@testable import ListingViewer
@testable import ListingAPI

private final class MockSplashView: SplashViewProtocol {

    var failExpectation: XCTestExpectation?
    var animateExpectation: XCTestExpectation?

    func showError(_ errorModel: ErrorViewState) {
        failExpectation?.fulfill()
    }

    func animateOut() -> Guarantee<UIViewAnimatingPosition> {
        animateExpectation?.fulfill()
        return after(seconds: 1).map { UIViewAnimatingPosition.end }
    }
}

/// This is a weakness of my current MVP approach to test some of this stuff, we end up stubbing out the world.
private final class MockListingBroker: ListingBrokerProtocol {

    let shouldFail: Bool

    init(shouldFail: Bool) {
        self.shouldFail = shouldFail
    }

    func rootCategory() -> Promise<ListingAPI.Category> {
        guard !shouldFail else {
            return Promise(error: TMError.unknown)
        }

        return after(seconds: 1).map {
            ListingAPI.Category(name: "Root",
                                       number: "0000",
                                       subcategories: [],
                                       isLeaf: false)
        }
    }
    
    func categoryWith(identifier: String) -> Promise<ListingAPI.Category> {
        fatalError("Not implemented")
    }
    
    func listingsForCategory(_ categoryId: String) -> Promise<SearchResult> {
        fatalError("Not implemented")
    }
    
    func listingResultImage(for url: URL) -> Promise<UIImage> {
        fatalError("Not implemented")
    }
    
    func listingDetailFor(identifier: Int) -> Promise<ListingDetail> {
        fatalError("Not implemented")
    }
}

private final class MockFlow: SplashFlow {

    var expectation: XCTestExpectation?

    func continueWithCategories(_ rootCategory: ListingAPI.Category) {
        expectation?.fulfill()
    }
}

class ListingViewerTests: XCTestCase {

    private var successBroker = MockListingBroker(shouldFail: false)
    private var failBroker = MockListingBroker(shouldFail: true)
    private var flow = MockFlow()
    private var view = MockSplashView()

    override func tearDown() {
        super.tearDown()
        flow = MockFlow()
    }

    func testSplashFlowSuccess() {
        let presenter = SplashPresenter(listingBroker: successBroker, flowDelegate: flow)
        presenter.view = view
        presenter.start()
        let expectation = XCTestExpectation(description: "Continue")
        let viewAnimation = XCTestExpectation(description: "ViewAnimation")
        flow.expectation = expectation
        view.animateExpectation = viewAnimation

        // We should hit the view to animate, then hit the flow to go to the next screen
        wait(for: [viewAnimation, expectation], timeout: 15)
    }

    func testSplashFlowFail() {
        let presenter = SplashPresenter(listingBroker: failBroker, flowDelegate: flow)
        presenter.view = view
        presenter.start()
        let errorExpectation = XCTestExpectation(description: "ShowError")
        view.failExpectation = errorExpectation
        wait(for: [errorExpectation], timeout: 15)
    }

}
