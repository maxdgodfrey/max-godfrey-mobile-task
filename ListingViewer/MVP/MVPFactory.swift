//
//  MVPFactory.swift
//  ListingViewer
//
//  Created by Max on 17/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit
import ListingAPI

internal final class MVPFactory {

    let brokerFactory: BrokerFactory

    init(brokerFactory: BrokerFactory) {
        self.brokerFactory = brokerFactory
    }

    public func buildSplash(coordinator: SplashFlow) -> SplashViewController {
        let splash: SplashViewController = UIStoryboard(storyboard: .splash).instantiateTypedViewController(withIdentifier: SplashViewController.className)
        let presenter = SplashPresenter(listingBroker: brokerFactory.listingBroker, flowDelegate: coordinator)
        splash.presenter = presenter
        presenter.view = splash
        return splash
    }

    func buildCategory(coordinator: CategoryFlow, identifier: String) -> CategoryViewController {
        return buildCategory(presenter: CategoryPresenter(listingBroker: brokerFactory.listingBroker, flow: coordinator, identifier: identifier))
    }

     func buildCategory(coordinator: CategoryFlow, category: ListingAPI.Category) -> CategoryViewController {
        return buildCategory(presenter: CategoryPresenter(listingBroker: brokerFactory.listingBroker, flow: coordinator, category: category))
    }

    private func buildCategory(presenter: CategoryPresenter) -> CategoryViewController {
        let categoryController: CategoryViewController = UIStoryboard(storyboard: .listing).instantiateTypedViewController(withIdentifier: CategoryViewController.className)
        categoryController.presenter = presenter
        presenter.view = categoryController
        return categoryController
    }

    public func buildListingResult(coordinator: ListingResultFlow, categoryIdentifier: String?, title: String) -> ListingResultViewController {
        let listingController: ListingResultViewController = UIStoryboard(storyboard: .listing).instantiateTypedViewController(withIdentifier: ListingResultViewController.className)
        let presenter = ListingResultPresenter(listingBroker: brokerFactory.listingBroker, flow: coordinator, categoryIdentifier: categoryIdentifier)
        listingController.presenter = presenter
        listingController.title = title
        presenter.view = listingController
        return listingController
    }

    public func buildListingDetail(coordinator: ListingDetailFlow, listingIdentifier: Int) -> ListingDetailViewController {
        let listingController: ListingDetailViewController = UIStoryboard(storyboard: .listing).instantiateTypedViewController(withIdentifier: ListingDetailViewController.className)
        let presenter = ListingDetailPresenter(listingBroker: brokerFactory.listingBroker, flow: coordinator, listingIdentifier: listingIdentifier)
        listingController.presenter = presenter
        presenter.view = listingController
        return listingController
    }
}
