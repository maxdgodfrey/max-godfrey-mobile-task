//
//  TMError+ErrorViewState.swift
//  ListingViewer
//
//  Created by Max on 20/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import ListingAPI

extension TMError {

    var displayMessage: String? {
        guard case let TMError.serverError(description) = self else {
            return nil
        }
        return description
    }
}
