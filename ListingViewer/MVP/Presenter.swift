//
//  Presenter.swift
//  ListingViewer
//
//  Created by Max on 17/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit

internal protocol Presenter: class {
    func start()
}

internal protocol MVPView: class {
    
}

internal protocol ErrorModelRendering {
    func showError(_ errorModel: ErrorViewState)
}

internal struct ErrorViewState {
    let title: String
    let message: String
    let reason: String? // The server provided reason if present. It's up to the consumer of this object if they want to render this.
    let actions: [(String, () -> Void)]

    init(title: String, message: String, reason: String? = nil, actions: [(String, () -> Void)]) {
        self.title = title
        self.message = message
        self.reason = reason
        self.actions = actions
    }
}

extension ErrorViewState {

    static let defaultAlert: ErrorViewState = {
        return ErrorViewState(title: "alert.error.generic.title".localized, message: "alert.error.generic.message".localized, actions: [("button.ok".localized, {})])
    }()

    static func defaultInline(with reason: String? = nil) -> ErrorViewState {
        return ErrorViewState(title: "alert.error.generic.title".localized,
                                     message: "alert.error.generic.message".localized,
                                     reason: reason,
                                     actions: [])
    }

    var defaultAlertController: UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actions = self.actions.map { action in
            UIAlertAction(title: action.0, style: .default, handler: { _ in action.1() })
        }
        actions.forEach { alertController.addAction($0) }
        return alertController
    }
}

/// Protocol for 
internal protocol FlowDelegate: class {

}
