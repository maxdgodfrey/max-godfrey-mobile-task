//
//  ServiceEndpoints.swift
//  ListingAPI
//
//  Created by Max on 20/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation

protocol URLPathable {
    var path: String { get }
}

internal enum AuthenticatedPaths {
    case rootCategories
    case categoryFor(identifier: String)
    case search
    case listingDetailWith(identifier: Int)
}

extension AuthenticatedPaths: URLPathable {

    /// This would be an ideal place to have, or at least hand off to string escaping logic.
    var path: String {
        switch self {
        case .rootCategories:
            return "/Categories/0.json"
        case .categoryFor(let identifier):
            return "/Categories/\(identifier).json"
        case .search:
            return "/Search/General.json"
        case .listingDetailWith(let identifier):
            return "/Listings/\(identifier).json"
        }
    }
}
