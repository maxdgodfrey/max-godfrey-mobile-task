//
//  MockListingService.swift
//  ListingAPI
//
//  Created by Max on 17/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation
import PromiseKit

internal class MockCategoryService: ListingServiceProtocol {

    /// Returns a Mock image, after 2 seconds, with a literal image of the end of the URL we are trying to load
    /// Useful to verify image loading behavior is correct
    func getImage(url: URL) -> Promise<UIImage> {
        return firstly {
            after(seconds: 2)
        }.then {
            Promise { seal in
                let label = UILabel(frame: CGRect(origin: .zero, size: CGSize(width: 42.5, height: 32)))
                label.text = url.lastPathComponent
                label.backgroundColor = #colorLiteral(red: 0.0700000003, green: 0.5350000262, blue: 0.8460000157, alpha: 1)
                label.textColor = #colorLiteral(red: 0.9752148986, green: 0.6859870553, blue: 0.1725027859, alpha: 1)
                label.font = UIFont.systemFont(ofSize: 7)
                seal.fulfill(UIImage.imageWith(view: label))
            }
        }
    }

    func getSearchResults(for category: String) -> Promise<SearchResult> {
        return decodePromiseFromJSONString(mockRootSearch)
    }

    func getListingDetailWith(identifier: Int) -> Promise<ListingDetail> {
        return decodePromiseFromJSONString(mockListingDetail)
    }

    func getCategoryWith(identifier: String) -> Promise<Category> {
        // Once we reach depth where the id > 6 we will return errors to excersize the views.
        if identifier.count > 6 {
            return Promise { seal in
                seal.fulfill(Category(name: "Test",
                                      number: "Test",
                                      subcategories: [],
                                      isLeaf: true))
            }
        }
        return decodePromiseFromJSONString(mockClothingAndFashion)
    }

    func getRootCategories() -> Promise<Category> {
        return decodePromiseFromJSONString(mockCategoriesRootDepthOne)
    }

    private func decodePromiseFromJSONString<A: Decodable>(_ json: String) -> Promise<A> {
        return firstly {
            after(seconds: 3)
        }.then {
            Promise { seal in
                seal.fulfill(try ListingAPIDecoding.defaultDecoder.decode(A.self, from: json.utf8Data))
            }
        }
    }
}

let mockListingDetail: String = """
    {
    "ListingId": 6527245,
    "Title": "Abstract 87",
    "Category": "0339-0066-5852-6500-",
    "StartPrice": 341.0,
    "BuyNowPrice": 389.0,
    "StartDate": "/Date(1520909174647)/",
    "EndDate": "/Date(1521427500000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521426716290)/",
    "CategoryPath": "/Art/Paintings/Abstract/Mixed-media",
    "PhotoId": 1078018,
    "RegionId": 11,
    "Region": "Wairarapa",
    "SuburbId": 62,
    "Suburb": "Martinborough",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "CategoryName": "Mixed media",
    "ReserveState": 2,
    "Attributes": [],
    "OpenHomes": [],
    "MinimumNextBidAmount": 341.0,
    "PriceDisplay": "$341.00",
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    },
    "Member": {
    "MemberId": 4000129,
    "Nickname": "abstractus",
    "DateAddressVerified": "/Date(1380798000000)/",
    "DateJoined": "/Date(1317726000000)/",
    "UniqueNegative": 3,
    "UniquePositive": 40,
    "FeedbackCount": 37,
    "IsAddressVerified": true,
    "Suburb": "Martinborough",
    "Region": "Wairarapa",
    "IsAuthenticated": true,
    "IsInTrade": true
    },
    "Body":
    "Abstract 87 ©\\u000d\\u000aStart  =  341.00\\u000d\\u000aReserve  =  379.00\\u000d\\u000aBuy Now  = 389.00",
    "Photos": [
    {
    "Key": 1078018,
    "Value": {
    "Thumbnail":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1078018.jpg",
    "List": "https://images.tmsandbox.co.nz/photoserver/lv2/1078018.jpg",
    "Medium": "https://images.tmsandbox.co.nz/photoserver/med/1078018.jpg",
    "Gallery": "https://images.tmsandbox.co.nz/photoserver/gv/1078018.jpg",
    "Large": "https://images.tmsandbox.co.nz/photoserver/tq/1078018.jpg",
    "FullSize":
    "https://images.tmsandbox.co.nz/photoserver/full/1078018.jpg",
    "PhotoId": 1078018,
    "OriginalWidth": 606,
    "OriginalHeight": 650
    }
    }
    ],
    "AllowsPickups": 1,
    "ShippingOptions": [
    {
    "Type": 4,
    "Price": 6.25,
    "Method": "North Island courier",
    "ShippingId": 4
    },
    {
    "Type": 4,
    "Price": 9.75,
    "Method": "South Island courier",
    "ShippingId": 5
    },
    {
    "Type": 2,
    "Price": 0,
    "Method": "I intend to pick-up",
    "ShippingId": 2
    }
    ],
    "PaymentOptions": "Cash",
    "IsInTradeProtected": true,
    "CanAddToCart": false,
    "EmbeddedContent": {},
    "SupportsQuestionsAndAnswers": true,
    "PaymentMethods": [
    {
    "Id": 4,
    "Name": "Cash"
    }
    ]
    }
"""

let mockRootSearch: String = """
    {
    "TotalCount": 17378,
    "Page": 1,
    "PageSize": 50,
    "List": [
    {
    "ListingId": 6532235,
    "Title": "MyProduct MP37 0012",
    "Category": "0004-0371-3448-5882-",
    "StartPrice": 0.5,
    "BuyNowPrice": 24.12,
    "StartDate": "/Date(1520994053750)/",
    "EndDate": "/Date(1521339653750)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Home-living/Kitchen/Cups-glasses/Glasses-tumblers",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/845381.jpg",
    "IsNew": true,
    "Region": "Auckland",
    "Suburb": "Auckland City",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "PriceDisplay": "$0.50",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6513338,
    "Title": "8GB SD Card",
    "Category": "0124-3424-3427-7085-",
    "StartPrice": 1.0,
    "BuyNowPrice": 10.0,
    "StartDate": "/Date(1520492478197)/",
    "EndDate": "/Date(1521356400000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Electronics-photography/Memory-cards/SD-media/8G",
    "IsNew": true,
    "Region": "Wellington",
    "Suburb": "Wellington City",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "PriceDisplay": "$1.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6526481,
    "Title": "Product Org17-19",
    "Category": "0340-2650-2655-",
    "StartPrice": 19.19,
    "BuyNowPrice": 75.19,
    "StartDate": "/Date(1520846124153)/",
    "EndDate": "/Date(1521364524153)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Pottery-glass/Glass-crystal/Plates",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1297195.jpg",
    "HasPayNow": true,
    "Region": "Southland",
    "Suburb": "Bluff",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$19.19",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6527460,
    "Title": "Tester Crimes",
    "Category": "0341-0886-1757-6535-",
    "StartPrice": 24.0,
    "BuyNowPrice": 31.0,
    "StartDate": "/Date(1520936467783)/",
    "EndDate": "/Date(1521368467783)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Crafts/Scrapbooking/Stickers/Cartoons",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/896545.jpg",
    "Region": "Canterbury",
    "Suburb": "Christchurch City",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "Subtitle": "Cartoon 1",
    "PriceDisplay": "$24.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6520563,
    "Title": "Product Org17-11",
    "Category": "0124-0060-7089-",
    "StartPrice": 11.11,
    "BuyNowPrice": 27.11,
    "StartDate": "/Date(1520764214340)/",
    "EndDate": "/Date(1521368940000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Electronics-photography/TVs/Wall-brackets",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1297188.jpg",
    "HasPayNow": true,
    "Region": "Southland",
    "Suburb": "Bluff",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$11.11",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6541268,
    "Title": "Pro1 Beta 0006",
    "Category": "0347-0189-6802-",
    "StartPrice": 9.0,
    "BuyNowPrice": 9.2,
    "StartDate": "/Date(1521223338953)/",
    "EndDate": "/Date(1521396060000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Toys-models/Educational-toys/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/3160362.jpg",
    "HasPayNow": true,
    "IsNew": true,
    "Region": "Otago",
    "Suburb": "Dunedin",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "PriceDisplay": "$9.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6528057,
    "Title": "Product Org17-17 Mx7",
    "Category": "0005-0387-4110-7134-",
    "StartPrice": 0,
    "BuyNowPrice": 63.17,
    "StartDate": "/Date(1520968412710)/",
    "EndDate": "/Date(1521400412710)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Sports/Camping-outdoors/Stretchers-mattresses/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1297194.jpg",
    "HasPayNow": true,
    "Region": "Southland",
    "Suburb": "Bluff",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 3,
    "Subtitle": "Multiple x7",
    "IsBuyNowOnly": true,
    "PriceDisplay": "$63.17 per item",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6520970,
    "Title": "Product Org17-14",
    "Category": "0004-0371-0609-",
    "StartPrice": 14.14,
    "BuyNowPrice": 45.14,
    "StartDate": "/Date(1520796856093)/",
    "EndDate": "/Date(1521401580000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Home-living/Kitchen/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1297191.jpg",
    "HasPayNow": true,
    "Region": "Southland",
    "Suburb": "Bluff",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$14.14",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6541625,
    "Title": "Junk Drunk Droll Troll Doll 39 J1B B2",
    "Category": "0187-2192-2676-",
    "StartPrice": 1.12,
    "BuyNowPrice": 12.02,
    "StartDate": "/Date(1521231286637)/",
    "EndDate": "/Date(1521404086637)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Antiques-collectables/Art-deco-retro/Fashion-beauty",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/892344.jpg",
    "IsNew": true,
    "Region": "Southland",
    "Suburb": "Invercargill",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "Subtitle": "J1B F2d",
    "PriceDisplay": "$1.12",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6528706,
    "Title": "Carbon Credit - zero flatulence",
    "Category": "0010-6327-",
    "StartPrice": 0,
    "StartDate": "/Date(1520972710360)/",
    "EndDate": "/Date(1521404710360)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Business-farming-industry/Carbon-credits",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/893950.jpg",
    "Region": "Auckland",
    "Suburb": "Auckland City",
    "NoteDate": "/Date(0)/",
    "ReserveState": 3,
    "IsClassified": true,
    "Subtitle": "Classified listing",
    "PriceDisplay": "Asking price: $800.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6541638,
    "Title": "MyProd MP26-112",
    "Category": "0004-0371-3448-5882-",
    "StartPrice": 100.12,
    "BuyNowPrice": 122.12,
    "StartDate": "/Date(1521234527217)/",
    "EndDate": "/Date(1521407327217)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Home-living/Kitchen/Cups-glasses/Glasses-tumblers",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/900518.jpg",
    "Region": "Wellington",
    "Suburb": "Wellington City",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$100.12",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6541647,
    "Title": "Test feature",
    "Category": "0339-3849-",
    "StartPrice": 1000.0,
    "StartDate": "/Date(1521237106503)/",
    "EndDate": "/Date(1521409906503)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Art/Drawings",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/3529603.jpg",
    "IsNew": true,
    "Region": "Wellington",
    "Suburb": "Wellington City",
    "NoteDate": "/Date(0)/",
    "PriceDisplay": "$1,000",
    "PhotoUrls": [
    "https://images.tmsandbox.co.nz/photoserver/thumb/3529602.jpg",
    "https://images.tmsandbox.co.nz/photoserver/thumb/3529601.jpg"
    ],
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6541648,
    "Title": "Product 270",
    "Category": "0347-3237-3239-",
    "StartPrice": 0,
    "BuyNowPrice": 29.0,
    "StartDate": "/Date(1521237106643)/",
    "EndDate": "/Date(1521409906643)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Toys-models/Bears/Collectable",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1072430.jpg",
    "Region": "Canterbury",
    "Suburb": "Christchurch City",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 3,
    "IsBuyNowOnly": true,
    "PriceDisplay": "$29.00 per item",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6522570,
    "Title": "Toolman - Cordless Grinder 01",
    "Category": "5964-5999-6015-6017-",
    "StartPrice": 35.0,
    "BuyNowPrice": 39.01,
    "StartDate": "/Date(1520807178917)/",
    "EndDate": "/Date(1521411900000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Building-renovation/Tools/Power-tools/Grinders",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/839632.jpg",
    "Region": "Auckland",
    "Suburb": "Auckland City",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "PriceDisplay": "$35.00",
    "HasFreeShipping": true,
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6526911,
    "Title": "Abstract 62",
    "Category": "0339-0066-5852-6502-",
    "StartPrice": 270.57,
    "BuyNowPrice": 327.0,
    "StartDate": "/Date(1520894475147)/",
    "EndDate": "/Date(1521412800000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Art/Paintings/Abstract/Oil",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1122428.jpg",
    "IsNew": true,
    "Region": "Wairarapa",
    "Suburb": "Martinborough",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$270.57",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6536928,
    "Title": "Product Org17-02",
    "Category": "0004-0374-0571-",
    "StartPrice": 252.5,
    "BuyNowPrice": 388.02,
    "StartDate": "/Date(1521155278843)/",
    "EndDate": "/Date(1521414478843)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Home-living/Lounge-dining-hall/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1297179.jpg",
    "HasPayNow": true,
    "IsNew": true,
    "Region": "Southland",
    "Suburb": "Bluff",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$252.50",
    "PromotionId": 4,
    "HasEmbeddedVideo": true,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6537052,
    "Title": "Product Org17-09",
    "Category": "0340-2650-2654-7531-",
    "StartPrice": 10450.44,
    "BuyNowPrice": 17342.09,
    "StartDate": "/Date(1521155914653)/",
    "EndDate": "/Date(1521415114653)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Pottery-glass/Glass-crystal/Ornaments/Paperweights",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1297186.jpg",
    "IsNew": true,
    "Region": "Southland",
    "Suburb": "Bluff",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$10,450",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6541700,
    "Title": "Org18-01 - Product 01",
    "Category": "0005-8947-8948-",
    "StartPrice": 800.0,
    "BuyNowPrice": 990.0,
    "StartDate": "/Date(1521245075517)/",
    "EndDate": "/Date(1521417875517)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Sports/Paintball/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1295929.jpg",
    "IsNew": true,
    "Region": "Southland",
    "Suburb": "Tokanui",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$800.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6531368,
    "Title": "Abstract 41",
    "Category": "0339-0491-5859-",
    "StartPrice": 225.6,
    "BuyNowPrice": 280.59,
    "StartDate": "/Date(1520987480077)/",
    "EndDate": "/Date(1521419400000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Art/Photographs/Abstract",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1115558.jpg",
    "IsNew": true,
    "Region": "Wairarapa",
    "Suburb": "Martinborough",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$225.60",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6537879,
    "Title": "Product Org17-13",
    "Category": "0341-4416-4419-",
    "StartPrice": 13.13,
    "BuyNowPrice": 39.13,
    "StartDate": "/Date(1521160938547)/",
    "EndDate": "/Date(1521420060000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Crafts/Knitting-Weaving/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1297190.jpg",
    "HasPayNow": true,
    "Region": "Southland",
    "Suburb": "Bluff",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$13.13",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6537958,
    "Title": "Product Org17-20 Sx1",
    "Category": "5964-5984-5988-",
    "StartPrice": 20.2,
    "BuyNowPrice": 81.2,
    "StartDate": "/Date(1521161534403)/",
    "EndDate": "/Date(1521420660000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Building-renovation/Carpet-tiles-flooring/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1297196.jpg",
    "HasPayNow": true,
    "Region": "Southland",
    "Suburb": "Bluff",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "Subtitle": "Single item x1",
    "PriceDisplay": "$20.20",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6527245,
    "Title": "Abstract 87",
    "Category": "0339-0066-5852-6500-",
    "StartPrice": 341.0,
    "BuyNowPrice": 389.0,
    "StartDate": "/Date(1520909174647)/",
    "EndDate": "/Date(1521427500000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Art/Paintings/Abstract/Mixed-media",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1078018.jpg",
    "Region": "Wairarapa",
    "Suburb": "Martinborough",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$341.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6527249,
    "Title": "Abstract 63",
    "Category": "0339-0066-5852-6500-",
    "StartPrice": 274.0,
    "BuyNowPrice": 329.0,
    "StartDate": "/Date(1520910076173)/",
    "EndDate": "/Date(1521428400000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Art/Paintings/Abstract/Mixed-media",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1124494.jpg",
    "IsNew": true,
    "Region": "Wairarapa",
    "Suburb": "Martinborough",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$274.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6533994,
    "Title": "Trinket 93 by Junk",
    "Category": "0004-0371-3448-5882-",
    "StartPrice": 30.0,
    "BuyNowPrice": 39.0,
    "StartDate": "/Date(1521083575893)/",
    "EndDate": "/Date(1521429175893)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Home-living/Kitchen/Cups-glasses/Glasses-tumblers",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/2653146.jpg",
    "Region": "Canterbury",
    "Suburb": "Christchurch City",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "Subtitle": "F4d",
    "PriceDisplay": "$30.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6514042,
    "Title": "Bagpipes 249",
    "Category": "0343-0892-1122-8150-",
    "StartPrice": 100.0,
    "BuyNowPrice": 160.0,
    "StartDate": "/Date(1520577192340)/",
    "EndDate": "/Date(1521441192340)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Music-instruments/Instruments/Wind-instruments/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/894351.jpg",
    "Region": "Canterbury",
    "Suburb": "Christchurch City",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$100.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6514176,
    "Title": "Illiterate scribbling - Flapdoodle",
    "Category": "0187-3232-3234-",
    "StartPrice": 285.0,
    "BuyNowPrice": 345.0,
    "StartDate": "/Date(1520587924233)/",
    "EndDate": "/Date(1521451924233)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Antiques-collectables/Documents-maps/Maps",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/840727.jpg",
    "Region": "Wellington",
    "Suburb": "Wellington City",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "Subtitle": "Nonsense",
    "PriceDisplay": "$285.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6534335,
    "Title": "Org18-03 - Product 03",
    "Category": "0004-3859-8375-",
    "StartPrice": 1020.0,
    "BuyNowPrice": 1400.0,
    "StartDate": "/Date(1521110221617)/",
    "EndDate": "/Date(1521455821617)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Home-living/Party-festive-supplies/Halloween",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1295922.jpg",
    "Region": "Southland",
    "Suburb": "Tokanui",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$1,020",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6534349,
    "Title": "MyProduct MP37 0036",
    "Category": "5964-5990-5998-8602-",
    "StartPrice": 18.0,
    "BuyNowPrice": 71.36,
    "StartDate": "/Date(1521111733923)/",
    "EndDate": "/Date(1521457333923)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Building-renovation/Building-supplies/Timber/Fencing",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/845330.jpg",
    "IsNew": true,
    "Region": "Auckland",
    "Suburb": "Auckland City",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$18.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6540720,
    "Title": "Org18-02 - Product 02",
    "Category": "0004-0375-1512-3896-",
    "StartPrice": 900.0,
    "BuyNowPrice": 1020.0,
    "StartDate": "/Date(1521200514357)/",
    "EndDate": "/Date(1521459714357)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath":
    "/Home-living/Outdoor-garden-conservatory/Sculptures-garden-ornaments/Garden-sculpture",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1295921.jpg",
    "IsNew": true,
    "Region": "Southland",
    "Suburb": "Tokanui",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$900.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6542363,
    "Title": "Pro1 Delta 0006",
    "Category": "0347-0189-6802-",
    "StartPrice": 9.0,
    "BuyNowPrice": 9.8,
    "StartDate": "/Date(1521311174157)/",
    "EndDate": "/Date(1521483900000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Toys-models/Educational-toys/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/3160362.jpg",
    "HasPayNow": true,
    "IsNew": true,
    "Region": "Otago",
    "Suburb": "Dunedin",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "PriceDisplay": "$9.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6542384,
    "Title": "Pro1 Alpha 0006",
    "Category": "0347-0189-6802-",
    "StartPrice": 9.0,
    "BuyNowPrice": 17.78,
    "StartDate": "/Date(1521311779920)/",
    "EndDate": "/Date(1521484500000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Toys-models/Educational-toys/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/3160362.jpg",
    "HasPayNow": true,
    "IsNew": true,
    "Region": "Otago",
    "Suburb": "Dunedin",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "PriceDisplay": "$9.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6534577,
    "Title": "Cajun Smoked Dragons Breath",
    "Category": "0005-9799-",
    "StartPrice": 10.0,
    "BuyNowPrice": 39.0,
    "StartDate": "/Date(1521140267640)/",
    "EndDate": "/Date(1521485867640)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Sports/Sports-nutrition-supplements",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/2541529.jpg",
    "Region": "Canterbury",
    "Suburb": "Christchurch City",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$10.00",
    "PromotionId": 4,
    "HasEmbeddedVideo": true,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6542572,
    "Title": "Product Giz-007",
    "Category": "0124-2710-4759-",
    "StartPrice": 0,
    "BuyNowPrice": 12.07,
    "StartDate": "/Date(1521314376703)/",
    "EndDate": "/Date(1521487176703)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Electronics-photography/Batteries/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1295027.jpg",
    "Region": "Hawke's Bay",
    "Suburb": "Dannevirke",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 3,
    "Subtitle": "MQL",
    "IsBuyNowOnly": true,
    "PriceDisplay": "$12.07 per item",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6526795,
    "Title": "Product Org17-15",
    "Category": "0187-2192-2676-",
    "StartPrice": 15.15,
    "BuyNowPrice": 51.15,
    "StartDate": "/Date(1520883677540)/",
    "EndDate": "/Date(1521488400000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Antiques-collectables/Art-deco-retro/Fashion-beauty",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1297192.jpg",
    "HasPayNow": true,
    "Region": "Southland",
    "Suburb": "Bluff",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$15.15",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6543004,
    "Title": "Org18-08 - Product 08",
    "Category": "0187-4353-4363-4406-",
    "StartPrice": 666.0,
    "BuyNowPrice": 888.0,
    "StartDate": "/Date(1521317115193)/",
    "EndDate": "/Date(1521489840000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath":
    "/Antiques-collectables/Coins/New-Zealand-Predecimal/Collections",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1295925.jpg",
    "Region": "Southland",
    "Suburb": "Tokanui",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$666.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6533603,
    "Title": "Product Org17-20 Mx10",
    "Category": "5964-5984-5988-",
    "StartPrice": 0,
    "BuyNowPrice": 50.22,
    "StartDate": "/Date(1521062054107)/",
    "EndDate": "/Date(1521493980000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Building-renovation/Carpet-tiles-flooring/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1297196.jpg",
    "HasPayNow": true,
    "Region": "Southland",
    "Suburb": "Bluff",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 3,
    "Subtitle": "Multiple items x10",
    "IsBuyNowOnly": true,
    "PriceDisplay": "$50.22 per item",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6535871,
    "Title": "Product Giz-003",
    "Category": "0005-0387-4994-6473-",
    "StartPrice": 0,
    "BuyNowPrice": 14.29,
    "StartDate": "/Date(1521148828950)/",
    "EndDate": "/Date(1521494428950)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Sports/Camping-outdoors/Hydration/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1404806.jpg",
    "Region": "Hawke's Bay",
    "Suburb": "Dannevirke",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 3,
    "IsBuyNowOnly": true,
    "PriceDisplay": "$14.29 per item",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6543806,
    "Title": "Product Org17-05",
    "Category": "0187-2192-2675-",
    "StartPrice": 972.05,
    "BuyNowPrice": 972.05,
    "StartDate": "/Date(1521322273623)/",
    "EndDate": "/Date(1521495000000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Antiques-collectables/Art-deco-retro/Art",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1297182.jpg",
    "HasPayNow": true,
    "Region": "Southland",
    "Suburb": "Bluff",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "PriceDisplay": "$972.05",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6543920,
    "Title": "5 of Clubs",
    "Category": "0347-0920-2340-7717-",
    "StartPrice": 10.0,
    "StartDate": "/Date(1521322996823)/",
    "EndDate": "/Date(1521495720000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath":
    "/Toys-models/Games-puzzles-tricks/Card-games/Playing-cards",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1342323.jpg",
    "Region": "Wellington",
    "Suburb": "Wellington City",
    "HasReserve": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$10.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6526870,
    "Title": "Product Org17-21",
    "Category": "0124-0060-2269-",
    "StartPrice": 221.0,
    "BuyNowPrice": 500.0,
    "StartDate": "/Date(1520893083683)/",
    "EndDate": "/Date(1521497883683)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Electronics-photography/TVs/Remotes",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1297197.jpg",
    "HasPayNow": true,
    "Region": "Southland",
    "Suburb": "Bluff",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$221.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6544550,
    "Title": "Pro1 Chi 0006",
    "Category": "0347-0189-6802-",
    "StartPrice": 9.0,
    "BuyNowPrice": 16.37,
    "StartDate": "/Date(1521326840493)/",
    "EndDate": "/Date(1521499560000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Toys-models/Educational-toys/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/3160362.jpg",
    "HasPayNow": true,
    "IsNew": true,
    "Region": "Otago",
    "Suburb": "Dunedin",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "PriceDisplay": "$9.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6544578,
    "Title": "MyProduct MP36 0066",
    "Category": "0005-4947-7271-",
    "StartPrice": 33.0,
    "BuyNowPrice": 130.66,
    "StartDate": "/Date(1521326965103)/",
    "EndDate": "/Date(1521499765103)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Sports/Windsurfing/Sails",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1389098.jpg",
    "IsNew": true,
    "Region": "Otago",
    "Suburb": "Dunedin",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$33.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6527169,
    "Title": "Mens Fashionista Overcoat - L",
    "Category": "0153-0438-3720-",
    "StartPrice": 117.0,
    "BuyNowPrice": 137.0,
    "StartDate": "/Date(1520901257197)/",
    "EndDate": "/Date(1521505980000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Clothing-Fashion/Men/Jackets",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/893921.jpg",
    "IsNew": true,
    "Region": "Auckland",
    "Suburb": "Auckland City",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "Subtitle": "Extra = feature Combo",
    "PriceDisplay": "$117.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6537959,
    "Title": "Product E1-Aster-0337",
    "Category": "0347-1417-6174-",
    "StartPrice": 13.88,
    "BuyNowPrice": 81.0,
    "StartDate": "/Date(1521161534420)/",
    "EndDate": "/Date(1521507060000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Toys-models/Pretend-playing/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/2500969.jpg",
    "Region": "Waikato",
    "Suburb": "Hamilton",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$13.88",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6527210,
    "Title": "Abstract 07",
    "Category": "0339-0066-5851-7324-",
    "StartPrice": 160.5,
    "BuyNowPrice": 315.65,
    "StartDate": "/Date(1520905034790)/",
    "EndDate": "/Date(1521509760000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Art/Paintings/Flora-fauna/Other",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1078619.jpg",
    "IsNew": true,
    "Region": "Wairarapa",
    "Suburb": "Martinborough",
    "HasReserve": true,
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 2,
    "PriceDisplay": "$160.50",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6527353,
    "Title": "Indoor Plant",
    "Category": "0004-0375-0583-2354-",
    "StartPrice": 40.0,
    "BuyNowPrice": 60.0,
    "StartDate": "/Date(1520923871020)/",
    "EndDate": "/Date(1521528600000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath":
    "/Home-living/Outdoor-garden-conservatory/Plants-trees/Indoor",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/3745147.jpg",
    "IsNew": true,
    "Region": "Wellington",
    "Suburb": "Wellington City",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "PriceDisplay": "$40.00",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6527355,
    "Title": "Retro Baby Blue Kettle",
    "Category": "0004-0371-0045-2541-",
    "StartPrice": 30.0,
    "BuyNowPrice": 50.0,
    "StartDate": "/Date(1520926275027)/",
    "EndDate": "/Date(1521531000000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Home-living/Kitchen/Small-appliances/Kettles-jugs",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/3745067.jpg",
    "IsNew": true,
    "Region": "Wellington",
    "Suburb": "Wellington City",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "PriceDisplay": "$30.00",
    "HasFreeShipping": true,
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6527356,
    "Title": "The Sun Also Rises",
    "Category": "0193-0463-0808-4454-",
    "StartPrice": 0,
    "BuyNowPrice": 25.0,
    "StartDate": "/Date(1520926275027)/",
    "EndDate": "/Date(1521531000000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Books/Fiction-literature/Classics/Author-GI",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/3745081.jpg",
    "IsNew": true,
    "Region": "Wellington",
    "Suburb": "Wellington City",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 3,
    "Subtitle": "Ernest Hemingway",
    "IsBuyNowOnly": true,
    "PriceDisplay": "$25.00 per item",
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6527434,
    "Title": "Princess Leia Action Figure",
    "Category": "0347-5370-1355-7684-",
    "StartPrice": 0,
    "BuyNowPrice": 222.0,
    "StartDate": "/Date(1520932993417)/",
    "EndDate": "/Date(1521537720000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath":
    "/Toys-models/Figurines-miniatures/Star-Wars/30th-anniversary-collection",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/1395850.jpg",
    "Region": "Wellington",
    "Suburb": "Lower Hutt City",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "ReserveState": 3,
    "Subtitle": "Mint Condition",
    "IsBuyNowOnly": true,
    "PriceDisplay": "$222.00 per item",
    "HasFreeShipping": true,
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    },
    {
    "ListingId": 6527827,
    "Title": "Gucci Handbag 2015 Summer Collection",
    "Category": "0153-0439-3001-6489-",
    "StartPrice": 395.0,
    "BuyNowPrice": 425.0,
    "StartDate": "/Date(1520949859283)/",
    "EndDate": "/Date(1521554580000)/",
    "ListingLength": null,
    "IsFeatured": true,
    "HasGallery": true,
    "IsBold": true,
    "AsAt": "/Date(1521339419924)/",
    "CategoryPath": "/Clothing-Fashion/Women/Bags-handbags/Handbags",
    "PictureHref":
    "https://images.tmsandbox.co.nz/photoserver/thumb/3569904.jpg",
    "IsNew": true,
    "Region": "Wellington",
    "Suburb": "Wellington City",
    "HasBuyNow": true,
    "NoteDate": "/Date(0)/",
    "PriceDisplay": "$395.00",
    "HasFreeShipping": true,
    "PromotionId": 4,
    "AdditionalData": {
    "BulletPoints": [],
    "Tags": []
    }
    }
    ],
    "DidYouMean": "",
    "FoundCategories": [
    {
    "Count": 54,
    "Category": "0001-",
    "Name": "Trade Me Motors",
    "CategoryId": 1
    },
    {
    "Count": 6868,
    "Category": "0350-",
    "Name": "Trade Me Property",
    "CategoryId": 350
    },
    {
    "Count": 696,
    "Category": "0187-",
    "Name": "Antiques & collectables",
    "CategoryId": 187
    },
    {
    "Count": 288,
    "Category": "0339-",
    "Name": "Art",
    "CategoryId": 339
    },
    {
    "Count": 321,
    "Category": "0351-",
    "Name": "Baby gear",
    "CategoryId": 351
    },
    {
    "Count": 73,
    "Category": "0193-",
    "Name": "Books",
    "CategoryId": 193
    },
    {
    "Count": 155,
    "Category": "5964-",
    "Name": "Building & renovation",
    "CategoryId": 5964
    },
    {
    "Count": 381,
    "Category": "0010-",
    "Name": "Business, farming & industry",
    "CategoryId": 10
    },
    {
    "Count": 510,
    "Category": "0153-",
    "Name": "Clothing & Fashion",
    "CategoryId": 153
    },
    {
    "Count": 2842,
    "Category": "0002-",
    "Name": "Computers",
    "CategoryId": 2
    },
    {
    "Count": 122,
    "Category": "0341-",
    "Name": "Crafts",
    "CategoryId": 341
    },
    {
    "Count": 896,
    "Category": "0124-",
    "Name": "Electronics & photography",
    "CategoryId": 124
    },
    {
    "Count": 7,
    "Category": "0202-",
    "Name": "Gaming",
    "CategoryId": 202
    },
    {
    "Count": 94,
    "Category": "4798-",
    "Name": "Health & beauty",
    "CategoryId": 4798
    },
    {
    "Count": 2233,
    "Category": "0004-",
    "Name": "Home & living",
    "CategoryId": 4
    },
    {
    "Count": 252,
    "Category": "0246-",
    "Name": "Jewellery & watches",
    "CategoryId": 246
    },
    {
    "Count": 24,
    "Category": "0344-",
    "Name": "Mobile phones",
    "CategoryId": 344
    },
    {
    "Count": 15,
    "Category": "0003-",
    "Name": "Movies & TV",
    "CategoryId": 3
    },
    {
    "Count": 130,
    "Category": "0343-",
    "Name": "Music & instruments",
    "CategoryId": 343
    },
    {
    "Count": 14,
    "Category": "9425-",
    "Name": "Pets & animals",
    "CategoryId": 9425
    },
    {
    "Count": 195,
    "Category": "0340-",
    "Name": "Pottery & glass",
    "CategoryId": 340
    },
    {
    "Count": 3,
    "Category": "9334-",
    "Name": "Services",
    "CategoryId": 9334
    },
    {
    "Count": 372,
    "Category": "0005-",
    "Name": "Sports",
    "CategoryId": 5
    },
    {
    "Count": 835,
    "Category": "0347-",
    "Name": "Toys & models",
    "CategoryId": 347
    },
    {
    "Count": 2,
    "Category": "9374-",
    "Name": "Travel, events & activities",
    "CategoryId": 9374
    }
    ]
    }
"""

let mockClothingAndFashion: String = """
    {
    "Name": "Clothing & Fashion",
    "Number": "0153-",
    "Path": "/Clothing-Fashion",
    "Subcategories": [
    {
    "Name": "Boys",
    "Number": "0153-0435-",
    "Path": "/Clothing-Fashion/Boys",
    "CanHaveSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Girls",
    "Number": "0153-0436-",
    "Path": "/Clothing-Fashion/Girls",
    "CanHaveSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Men",
    "Number": "0153-0438-",
    "Path": "/Clothing-Fashion/Men",
    "CanHaveSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Women",
    "Number": "0153-0439-",
    "Path": "/Clothing-Fashion/Women",
    "CanHaveSecondCategory": true,
    "IsLeaf": false
    }
    ],
    "CanHaveSecondCategory": true,
    "IsLeaf": false
    }
"""

let mockCategoriesRootDepthOne: String = """
    {
    "Name": "Root",
    "Number": "",
    "Path": "",
    "Subcategories": [
    {
    "Name": "Trade Me Motors",
    "Number": "0001-",
    "Path": "/Trade-Me-Motors",
    "HasClassifieds": true,
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Trade Me Property",
    "Number": "0350-",
    "Path": "/Trade-Me-Property",
    "HasClassifieds": true,
    "IsLeaf": false
    },
    {
    "Name": "Trade Me Jobs",
    "Number": "5000-",
    "Path": "/Trade-Me-Jobs",
    "HasClassifieds": true,
    "IsLeaf": false
    },
    {
    "Name": "Antiques & collectables",
    "Number": "0187-",
    "Path": "/Antiques-collectables",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Art",
    "Number": "0339-",
    "Path": "/Art",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Baby gear",
    "Number": "0351-",
    "Path": "/Baby-gear",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Books",
    "Number": "0193-",
    "Path": "/Books",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Building & renovation",
    "Number": "5964-",
    "Path": "/Building-renovation",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Business, farming & industry",
    "Number": "0010-",
    "Path": "/Business-farming-industry",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Clothing & Fashion",
    "Number": "0153-",
    "Path": "/Clothing-Fashion",
    "CanHaveSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Computers",
    "Number": "0002-",
    "Path": "/Computers",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Crafts",
    "Number": "0341-",
    "Path": "/Crafts",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Electronics & photography",
    "Number": "0124-",
    "Path": "/Electronics-photography",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Flatmates wanted",
    "Number": "2975-",
    "Path": "/Flatmates-wanted",
    "HasClassifieds": true,
    "AreaOfBusiness": 1,
    "IsLeaf": true
    },
    {
    "Name": "Gaming",
    "Number": "0202-",
    "Path": "/Gaming",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Health & beauty",
    "Number": "4798-",
    "Path": "/Health-beauty",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Home & living",
    "Number": "0004-",
    "Path": "/Home-living",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Jewellery & watches",
    "Number": "0246-",
    "Path": "/Jewellery-watches",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Mobile phones",
    "Number": "0344-",
    "Path": "/Mobile-phones",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Movies & TV",
    "Number": "0003-",
    "Path": "/Movies-TV",
    "CanHaveSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Music & instruments",
    "Number": "0343-",
    "Path": "/Music-instruments",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Pets & animals",
    "Number": "9425-",
    "Path": "/Pets-animals",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Pottery & glass",
    "Number": "0340-",
    "Path": "/Pottery-glass",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Services",
    "Number": "9334-",
    "Path": "/Services",
    "HasClassifieds": true,
    "IsLeaf": false
    },
    {
    "Name": "Sports",
    "Number": "0005-",
    "Path": "/Sports",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Toys & models",
    "Number": "0347-",
    "Path": "/Toys-models",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    },
    {
    "Name": "Travel, events & activities",
    "Number": "9374-",
    "Path": "/Travel-events-activities",
    "CanHaveSecondCategory": true,
    "CanBeSecondCategory": true,
    "IsLeaf": false
    }
    ],
    "IsLeaf": false
    }
"""
