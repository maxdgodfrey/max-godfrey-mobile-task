//
//  Webservice.swift
//  ListingAPI
//
//  Created by Max on 18/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation
import PromiseKit
import Alamofire

final class Webservice {

    let session: URLSession
    let base: URL
    let errorAdapter: ErrorAdapter

    init(session: URLSession, base: URL, errorAdapter: ErrorAdapter) {
        self.session = session
        self.base = base
        self.errorAdapter = errorAdapter
    }

    func request<A: Decodable>(path: String, method: HTTPMethod, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) -> Promise<A> {
        guard let url = path.url(relativeTo: base) else {
            preconditionFailure("Programmer error, can't construct url with base:\(base)")
        }
        return firstly {
            Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
                .validate(errorAdapter.validation)
                .responseDecodable(decoder: ListingAPIDecoding.defaultDecoder)
        }
    }
}

extension String {

    func url(relativeTo base: URL) -> URL? {
        var urlComps = URLComponents()
        urlComps.path = base.path + self
        return urlComps.url(relativeTo: base)
    }
}
