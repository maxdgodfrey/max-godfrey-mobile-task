//
//  ListingService.swift
//  ListingViewer
//
//  Created by Max on 16/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation
import PromiseKit
import Alamofire
import AlamofireImage

internal protocol ListingServiceProtocol {

    /// Return the root of the category tree.
    func getRootCategories() -> Promise<Category>

    /// Get a category with identifier
    ///
    /// - Parameter identifier: A category identifier, of the form "XXXX-...XXXX-"
    /// - Returns: A promise resolving with the node for that identifier in the category tree
    func getCategoryWith(identifier: String) -> Promise<Category>

    /// Gets the search results for a supplied category identifier
    func getSearchResults(for categoryIdentifier: String) -> Promise<SearchResult>

    /// Gets a listing detail object for the supplied listing identifier.
    func getListingDetailWith(identifier: Int) -> Promise<ListingDetail>

    /// Fetch an image for a fully qualified URL. The result will be cached, for more see the documentation of the AlamofireImage
    func getImage(url: URL) -> Promise<UIImage>
}

internal class ListingService: ListingServiceProtocol {

    let webservice: Webservice
    let imageService: ImageDownloader

    init(webservice: Webservice, imageService: ImageDownloader) {
        self.webservice = webservice
        self.imageService = imageService
    }

    /*
     This is not ideal:
     - Best case there would be actual OAuth token refresh logic happening. The `webservice` that is injected could instead be behind a protocol, then the concrete implementation could decide and manage getting a refresh/auth token if needed.
     - A worse but still better would be to obscure these so that a `strings` command would not immediately find them, e.g. encode them as a binary array or something to that effect, still insecure.
     - Time hasn't allowed this, but I am very aware that is insecure.
    */
    let authHeaders: HTTPHeaders = {
        return ["Authorization": """
                OAuth oauth_consumer_key="A1AC63F0332A131A78FAC304D007E7D1", oauth_signature_method="PLAINTEXT", oauth_signature="EC7F18B17A062962C6930A8AE88B16C7%26"
                """]
    }()

    func getRootCategories() -> Promise<Category> {
        let parameters = ["depth": "1"]
        return webservice.request(path: AuthenticatedPaths.rootCategories.path,
                                  method: .get,
                                  parameters: parameters,
                                  encoding: URLEncoding.queryString)
    }

    func getCategoryWith(identifier: String) -> Promise<Category> {
        let parameters = ["depth": "1"]
        return webservice.request(path: AuthenticatedPaths.categoryFor(identifier: identifier).path,
                                  method: .get,
                                  parameters: parameters,
                                  encoding: URLEncoding.queryString)
    }

    func getSearchResults(for category: String) -> Promise<SearchResult> {
        let queries = ["rows": "20",
                       "return_did_you_mean": "false",
                       "category": category]
        return webservice.request(path: AuthenticatedPaths.search.path,
                                  method: .get,
                                  parameters: queries,
                                  encoding: URLEncoding.queryString,
                                  headers: authHeaders)
    }

    func getListingDetailWith(identifier: Int) -> Promise<ListingDetail> {
        let parameters: [String: Any] = ["question_limit": 0,
                          "return_member_profile": false]
        return webservice.request(path: AuthenticatedPaths.listingDetailWith(identifier: identifier).path,
                                  method: .get,
                                  parameters: parameters,
                                  encoding: URLEncoding.queryString,
                                  headers: authHeaders)
    }

    func getImage(url: URL) -> Promise<UIImage> {
        let urlRequest = URLRequest(url: url)
        let queue = DispatchQueue.global(qos: .utility)
        return imageService.download(urlRequest, progressQueue: queue)
    }
}
