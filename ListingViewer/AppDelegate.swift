//
//  AppDelegate.swift
//  ListingViewer
//
//  Created by Max on 16/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit
// swiftlint:disable force_cast
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var appCoordinator: AppCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        appCoordinator = AppCoordinator()
        UINavigationBar.appearance().tintColor = #colorLiteral(red: 0.0700000003, green: 0.5350000262, blue: 0.8460000157, alpha: 1)
        UIButton.appearance().tintColor = #colorLiteral(red: 0.0700000003, green: 0.5350000262, blue: 0.8460000157, alpha: 1)
        return true
    }
}
