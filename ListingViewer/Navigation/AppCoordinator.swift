//
//  AppCoordinator.swift
//  ListingViewer
//
//  Created by Max on 17/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit
import ListingAPI

internal final class AppCoordinator: NSObject {

    private let window: UIWindow = UIWindow(frame: UIScreen.main.bounds)
    private let rootNavigationController: UINavigationController
    private let categoryNavigationController: UINavigationController

    private let brokerFactory: BrokerFactory
    private let mvpFactory: MVPFactory

    private var rootCategory: ListingAPI.Category?

    var listingViewController: UIViewController? {
        return rootNavigationController.viewControllers.flatMap { $0 as? ListingResultViewController }.first
    }

    override init() {
        let navController = UINavigationController()
        navController.setNavigationBarHidden(true, animated: false)
        self.rootNavigationController = navController
        self.categoryNavigationController = UINavigationController()

        brokerFactory = BrokerFactory()
        mvpFactory = MVPFactory(brokerFactory: brokerFactory)
        super.init()
        window.rootViewController = navController
        navController.setViewControllers([mvpFactory.buildSplash(coordinator: self)], animated: false)

        window.makeKeyAndVisible()
    }
}

extension AppCoordinator: ErrorModelRendering {

    func showError(_ errorModel: ErrorViewState) {
        self.window.rootViewController?.presentedViewController?.present(errorModel.defaultAlertController, animated: true, completion: nil)
    }
}

extension AppCoordinator: SplashFlow {

    func continueWithCategories(_ rootCategory: ListingAPI.Category) {
        let listingController = mvpFactory.buildListingResult(coordinator: self, categoryIdentifier: rootCategory.number, title: "All Caterogies")
        self.rootCategory = rootCategory
        rootNavigationController.setNavigationBarHidden(false, animated: true)
        rootNavigationController.setViewControllers([listingController], animated: false)
    }
}

extension AppCoordinator: CategoryFlow {

    func showListings(_ categoryIdentifier: String, categoryName: String) {
        let vc = mvpFactory.buildListingResult(coordinator: self, categoryIdentifier: categoryIdentifier, title: categoryName)
        rootNavigationController.setViewControllers([vc], animated: false)
    }

    func showCategoryWith(identifier: String) {
        let vc = mvpFactory.buildCategory(coordinator: self, identifier: identifier)
        categoryNavigationController.pushViewController(vc, animated: true)
    }

    func dismissCategories() {
        categoryNavigationController.dismiss(animated: true)
    }

    func back() {
        categoryNavigationController.popViewController(animated: true)
    }
}

extension AppCoordinator: ListingResultFlow {

    func showCategories() {
        guard let rootCategory = rootCategory else {
            return
        }
        if categoryNavigationController.viewControllers.isEmpty {
            let categoryViewController = mvpFactory.buildCategory(coordinator: self, category: rootCategory)
            categoryNavigationController.setViewControllers([categoryViewController], animated: false)
        }

        categoryNavigationController.modalPresentationStyle = .popover
        categoryNavigationController.popoverPresentationController?.barButtonItem =
            listingViewController?.navigationItem.leftBarButtonItem
        categoryNavigationController.popoverPresentationController?.delegate = self
        window.rootViewController?.present(categoryNavigationController, animated: false, completion: nil)
    }

    func showListingDetail(for listingId: Int) {
        rootNavigationController.pushViewController(mvpFactory.buildListingDetail(coordinator: self, listingIdentifier: listingId), animated: true)
    }
}

extension AppCoordinator: ListingDetailFlow {

}

extension AppCoordinator: UIPopoverPresentationControllerDelegate {

    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        switch traitCollection.horizontalSizeClass {
        case .regular:
            return .popover
        case .compact, .unspecified:
            return .none
        }
    }

    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
