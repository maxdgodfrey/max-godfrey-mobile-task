//
//  StoryboardSafety.swift
//  ListingViewer
//
//  Created by Max on 17/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit

internal enum Storyboard: String {
    case splash
    case listing

    var filename: String {
        return self.rawValue.capitalized
    }
}

extension NSObject {
    class var className: String {
        return String(describing: self)
    }

    var className: String {
        return String(describing: type(of: self))
    }
}

extension UIStoryboard {

    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.filename, bundle: bundle)
    }

    func instantiateTypedViewController<A: UIViewController>(withIdentifier identifier: String) -> A {
        guard let result = self.instantiateViewController(withIdentifier: identifier) as? A else {
            preconditionFailure("Programmer error, tried to load a controller with identifier \(identifier)")
        }
        return result
    }
}
