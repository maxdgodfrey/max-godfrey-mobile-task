//
//  ViewExtensiosn.swift
//  ListingViewer
//
//  Created by Max on 20/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit

extension UIRefreshControl {

    convenience public init(tintColor: UIColor, target: Any?, action: Selector) {
        self.init(frame:  CGRect(origin: .zero, size: .refreshControlSize))
        self.tintColor = tintColor
        addTarget(target, action: action, for: .valueChanged)
    }
}

extension UILabel {

    /// Sets the labels visibilty and text based on the optional String provided. If the string is nil, we hide the view otherwise we don't adjust
    /// it visibilty and show the text.
    ///
    /// - Parameter text: An optional String to show in this label.
    func setTextOrHide(_ text: String?) {
        guard let text = text else {
            isHidden = true
            return
        }
        self.text = text
    }
}

extension UIView {

    func animateIn(with animator: UIViewPropertyAnimator = .default) {
        fadeAnimate(in: true, animator: animator)
    }

    func animateOut(with animator: UIViewPropertyAnimator = .default) {
        fadeAnimate(in: false, animator: animator)
    }

    private func fadeAnimate(in shouldAppear: Bool, animator: UIViewPropertyAnimator) {
        guard animator.state != .stopped else {
            return // Ignore animators that aren't complete for safety
        }
        alpha = shouldAppear ? 0 : 1
        animator.addAnimations {
            self.alpha = shouldAppear ? 1 : 0
        }
        animator.startAnimation()
    }
}

extension UIViewPropertyAnimator {

    static var `default`: UIViewPropertyAnimator {
        return UIViewPropertyAnimator(duration: .defaultAnimationFadeDuration, curve: .easeInOut)
    }
}
