//
//  StringExtensiosn.swift
//  ListingViewer
//
//  Created by Max on 17/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation

extension String {

    /// Convenience for getting a localized string, also makes for a very nice call site.
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
