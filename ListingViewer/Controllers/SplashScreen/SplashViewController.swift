//
//  SplashViewController.swift
//  ListingViewer
//
//  Created by Max on 17/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit
import PromiseKit

internal protocol SplashViewProtocol: MVPView, ErrorModelRendering {

    /// Notify the view it should "animate out", we are about to leave this screen.
    ///
    /// - Returns: A guarantee that resolves once the animation completes. 
    func animateOut() -> Guarantee<UIViewAnimatingPosition>
}

internal class SplashViewController: UIViewController {

    @IBOutlet weak var featureLabel: UILabel!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    var presenter: SplashPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.start()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let loadingAppear = UIViewPropertyAnimator(duration: .splashScreenAnimationDuration, dampingRatio: .splashScreenDampingRatio) {
            self.loadingView.isHidden = false
        }
        loadingAppear.startAnimation(afterDelay: .splashScreenAnimationDelay)
    }
}

extension SplashViewController: SplashViewProtocol {

    func animateOut() -> Guarantee<UIViewAnimatingPosition> {
        return UIViewPropertyAnimator(duration: .splashScreenAnimationFadeOutDuration, curve: .easeInOut) {
            self.featureLabel.alpha = 0
            self.loadingView.alpha = 0
            self.view.backgroundColor = UIColor.groupTableViewBackground
        }.startAnimation(.promise)
    }

    func showError(_ errorModel: ErrorViewState) {
        present(errorModel.defaultAlertController, animated: true, completion: nil)
    }
}
