//
//  SplashPresenter.swift
//  ListingViewer
//
//  Created by Max on 17/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation
import ListingAPI
import PromiseKit
import UIKit

/// Everywhere where this screen knows how to go
internal protocol SplashFlow: FlowDelegate {
    func continueWithCategories(_ rootCategory: ListingAPI.Category)
}

internal protocol SplashPresenterProtocol: Presenter {

}

final internal class SplashPresenter {

    let listingBroker: ListingBrokerProtocol
    weak var view: SplashViewProtocol?
    weak var flowDelegate: SplashFlow?

    init(listingBroker: ListingBrokerProtocol, flowDelegate: SplashFlow) {
        self.listingBroker = listingBroker
        self.flowDelegate = flowDelegate
    }    
}

extension SplashPresenter: SplashPresenterProtocol {

    func start() {
        loadCategoriesAndProceed()
    }

    func loadCategoriesAndProceed() {
        guard let view = view else { return }

        listingBroker.rootCategory().then(on: DispatchQueue.main) { (category) -> Guarantee<ListingAPI.Category> in
            view.animateOut().map { _ in
                category
            }
        }.done { category in
            self.flowDelegate?.continueWithCategories(category)
        }.catch { _ in
                let tryAgainAction: (String, () -> Void) = ("button.tryAgain".localized, { [weak self] in
                    self?.loadCategoriesAndProceed()
                })
                self.view?.showError(ErrorViewState(title: "alert.error.generic.title".localized, message: "alert.error.generic.message".localized, actions: [tryAgainAction]))
        }
    }
}
