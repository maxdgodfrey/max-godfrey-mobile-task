//
//  ListingViewController.swift
//  ListingViewer
//
//  Created by Max on 18/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit
import PromiseKit

internal protocol ListingDetailViewProtocol: MVPView {
    var state: ListingDetailViewState { get set }
}

internal final class ListingDetailViewController: UIViewController {

    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var inlineErrorView: InlineErrorView!

    @IBOutlet weak var listingIdLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var suburbName: UILabel!
    @IBOutlet weak var regionName: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    @IBOutlet weak var contentContainer: UIScrollView!
    @IBOutlet weak var imageContainer: UIStackView!
    @IBOutlet weak var imageScrollView: UIScrollView!

    var presenter: ListingDetailPresenterProtocol?

    var state: ListingDetailViewState = .loading {
        didSet {
            loadingView.stopAnimating()
            loadingView.animateOut()

            switch state {
            case .loading:
                // we can only enter the loading state from pull to refresh here.
                 inlineErrorView.animateOut()
            case .data(let detail):
                contentContainer.refreshControl?.endRefreshing()
                inlineErrorView.animateOut()

                contentContainer.animateIn()
                showDetail(detail)
            case .error(let model):
                contentContainer.refreshControl?.endRefreshing()
                contentContainer.animateOut()

                inlineErrorView.animateIn()
                inlineErrorView.errorModel = model
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        contentContainer.refreshControl = UIRefreshControl(tintColor: .black, target: self, action: #selector(didPullToRefresh(_:)))

        // Set the view to a fullscreen loading state
        contentContainer.animateOut()
        loadingView.animateIn()
        loadingView.startAnimating()

        presenter?.start()
    }

    @objc
    func didPullToRefresh(_ sender: Any) {
        presenter?.refresh()
    }

    func showDetail(_ detail: ListingDetailViewState.Detail) {
        contentContainer.isHidden = false
        imageContainer.arrangedSubviews.forEach { $0.removeFromSuperview() } // Tidy up the image views that could have previously been added.

        titleLabel.text = detail.title
        listingIdLabel.text = detail.listingIdentifier

        subtitleLabel.setTextOrHide(detail.subtitle)
        categoryNameLabel.text = detail.categoryName
        regionName.setTextOrHide(detail.region)
        suburbName.setTextOrHide(detail.suburb)
        priceLabel.setTextOrHide(detail.priceDisplay)

        guard !detail.images.isEmpty else {
            imageScrollView.isHidden = true
            return
        }
        imageScrollView.isHidden = false
        detail.images.enumerated().forEach { offset, imagePromise in
            let imageView = UIImageView()
            imageView.contentMode = .scaleAspectFit
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageContainer.addArrangedSubview(imageView)
            NSLayoutConstraint.activate([
                imageView.widthAnchor.constraint(equalTo: imageScrollView.widthAnchor),
                imageView.heightAnchor.constraint(equalTo: imageScrollView.heightAnchor)
            ])

            firstly {
                imagePromise()
            }.done { image in
                guard offset < self.imageContainer.arrangedSubviews.count else {
                    return
                }
                (self.imageContainer.arrangedSubviews[offset] as? UIImageView)?.image = image
            }.catch { _ in
                self.imageContainer.arrangedSubviews[offset].isHidden = true
            }
        }
    }
}

extension ListingDetailViewController: ListingDetailViewProtocol {
    
}
