//
//  ListingPresenter.swift
//  ListingViewer
//
//  Created by Max on 18/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import ListingAPI
import PromiseKit

internal protocol ListingDetailFlow: FlowDelegate {

}

internal protocol ListingDetailPresenterProtocol: Presenter {

    func refresh()
}

internal enum ListingDetailViewState {
    case loading
    case error(ErrorViewState)
    case data(Detail)

    struct Detail {
        let listingIdentifier: String
        let title: String
        let subtitle: String?
        let region: String?
        let suburb: String?
        let priceDisplay: String?
        let categoryName: String
        let images: [() -> Promise<UIImage>]
    }
}

internal final class ListingDetailPresenter {

    weak var view: ListingDetailViewProtocol?
    weak var flow: ListingDetailFlow?

    let listingBroker: ListingBrokerProtocol
    let listingIdentifier: Int

    init(listingBroker: ListingBrokerProtocol, flow: ListingDetailFlow, listingIdentifier: Int) {
        self.listingBroker = listingBroker
        self.flow = flow
        self.listingIdentifier = listingIdentifier
    }
}

extension ListingDetailPresenter: ListingDetailPresenterProtocol {

    func start() {
        firstly {
            self.listingBroker.listingDetailFor(identifier: self.listingIdentifier)
        }.map { detail in
            ListingDetailPresenter.viewDetailFrom(listingDetail: detail,
                                                  identifier: self.listingIdentifier,
                                                  ListingBrokerProtocolProtocol: self.listingBroker)
        }.done { detail in
            self.view?.state = .data(detail)
        }.catch { error in
            self.view?.state = .error(.defaultInline(with: (error as? TMError)?.displayMessage))
        }
    }

    func refresh() {
        start()
    }

    static func viewDetailFrom(listingDetail: ListingDetail, identifier: Int, ListingBrokerProtocolProtocol: ListingBrokerProtocol) -> ListingDetailViewState.Detail {
        let images = (listingDetail.photos ?? []).map { photoModel in
            return {
                ListingBrokerProtocolProtocol.listingResultImage(for: photoModel.medium)
            }
        }

        return ListingDetailViewState.Detail(listingIdentifier: "\(identifier)",
                                             title: listingDetail.title,
                                             subtitle: listingDetail.subtitle,
                                             region: listingDetail.region,
                                             suburb: listingDetail.suburb,
                                             priceDisplay: listingDetail.priceDisplay,
                                             categoryName: listingDetail.category,
                                             images: images)
    }

}
