//
//  ListingResultCell.swift
//  ListingViewer
//
//  Created by Max on 18/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit
import PromiseKit

internal final class ListingResultCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var priceDisplayLabel: UILabel!

    // Used to decide if a resolving image load is for this instance, as we are dealing with reusable cells this lets us guard images appearing in the wrong cell
    var identifier: Int?

    var imageResolver: ListingImageResolver?

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
        layer.cornerRadius = .defaultCornerRadius
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        detailLabel.isHidden = false
        imageView.isHidden = false
        imageView.image = nil
        imageResolver = nil
        identifier = nil // By setting this to nil, we prevent the previous image load promise from resolving on me.
    }

    func setupWith(listing: ListingResultViewState.Listing) {
        identifier = listing.identifier
        titleLabel.text = listing.title
        detailLabel.setTextOrHide(listing.detail)
        priceDisplayLabel.setTextOrHide(listing.priceDisplay)

        // if we dont have an image to fetch, then collapse the view.
        guard let imageResolver = listing.thumbnailPromise else {
            imageView.isHidden = true
            return
        }

        self.imageResolver = imageResolver
        firstly {
            imageResolver()
        }.done { image, identifier in
            guard identifier == self.identifier else {
                throw ImageLoadError.imageIdMissmatch
            }
            self.imageView.image = image
        }.catch { _ in
            self.imageView.isHidden = true
        }

    }
}
