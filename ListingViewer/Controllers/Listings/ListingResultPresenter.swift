//
//  ListingPresenter.swift
//  ListingViewer
//
//  Created by Max on 18/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import ListingAPI
import PromiseKit

internal protocol ListingResultFlow: FlowDelegate, ErrorModelRendering {
    func showListingDetail(for listingId: Int)
    func showCategories()
}

internal protocol ListingResultPresenterProtocol: Presenter {
    func showCategories()
    func refresh()
    func didTapItemWith(model: ListingResultViewState.Listing)
}

internal enum ImageLoadError: Error {
    case imageIdMissmatch
}

typealias ListingImageResolver = () -> Promise<ImageLoadResult>
typealias ImageLoadResult = (image: UIImage, identifier: Int)

internal struct ListingResultViewState {

    let title: String?
    let state: Result

    enum Result {
        case loading
        case data([Listing])
        case error(ErrorViewState) // as we render errors at the cooridinator level, this is for rendering inline errors (e.g. no data/listings)
    }

    struct Listing {
        let identifier: Int
        let title: String
        let detail: String?
        let priceDisplay: String?
        let thumbnailPromise: ListingImageResolver?
    }
}

extension ListingResultViewState.Result {

    static var noDataError: ListingResultViewState.Result {
        return .error(ErrorViewState(title: "listing.nolistings.title".localized,
                                     message: "listing.nolistings.message".localized,
                                     actions: []))
    }
}

internal final class ListingResultPresenter {

    weak var view: ListingResultViewProtocol?
    weak var flow: ListingResultFlow?
    let listingBroker: ListingBrokerProtocol
    let categoryIdentifier: String?

    init(listingBroker: ListingBrokerProtocol, flow: ListingResultFlow, categoryIdentifier: String?) {
        self.listingBroker = listingBroker
        self.flow = flow
        self.categoryIdentifier = categoryIdentifier
    }
}

extension ListingResultPresenter: ListingResultPresenterProtocol {

    func start() {
        guard let categoryIdentifier = categoryIdentifier else {
            view?.state = .noDataError
            return
        }
        firstly {
            listingBroker.listingsForCategory(categoryIdentifier)
        }.map { searchResult in
            ListingResultPresenter.stateFrom(models: searchResult.listings, broker: self.listingBroker)
        }.done { viewState in
            self.view?.state = viewState
        }.catch { error in
            // Show an inline error,
            self.view?.state = .error(ErrorViewState(title: "listing.unknownError.title".localized,
                                                     message: "listing.unknownError.message".localized,
                                                     reason: (error as? TMError)?.displayMessage,
                                                     actions: [("Try again", { [weak self] in self?.start() })]))
            // and forward a more generic error to display.
            self.flow?.showError(.defaultAlert)
        }
    }

    func refresh() {
        start()
    }

    func showCategories() {
        flow?.showCategories()
    }

    func didTapItemWith(model: ListingResultViewState.Listing) {
        flow?.showListingDetail(for: model.identifier)
    }

    static func stateFrom(models: [ListingAPI.Listing], broker: ListingBrokerProtocol) -> ListingResultViewState.Result {
        guard !models.isEmpty else {
            return .noDataError
        }
        let results = models.map { model in
            ListingResultViewState.Listing(identifier: model.identifier,
                                           title: model.title,
                                           detail: model.subtitle,
                                           priceDisplay: model.priceDisplay,
                                           thumbnailPromise: thumbnailPromiseFor(model: model, broker: broker))
        }
        return .data(results)
    }

    static func thumbnailPromiseFor(model: ListingAPI.Listing, broker: ListingBrokerProtocol) -> ListingImageResolver? {
        // if there is no image, or we fail to construct a URL we should notify the UI to collapse
        guard let href = model.imageHref, let url = URL(string: href) else {
            return nil
        }
        // We wrap this in a closure so the UI can load this when it's ready.
        return {
            return firstly {
                broker.listingResultImage(for: url)
            }.map { image in
                // here we return a tuple of image, and identifier
                (image: image, identifier: model.identifier)
            }
        }
    }
}
