//
//  ListingViewController.swift
//  ListingViewer
//
//  Created by Max on 18/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit

internal protocol ListingResultViewProtocol: MVPView, ErrorModelRendering {

    var state: ListingResultViewState.Result { get set }
}

internal final class ListingResultViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    @IBOutlet weak var loadingView: UIActivityIndicatorView!

    @IBOutlet weak var inlineErrorView: InlineErrorView!

    var presenter: ListingResultPresenterProtocol?

    var datasource: ListingDatasource = ListingDatasource()
    var layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()

    var state: ListingResultViewState.Result = .loading {
        didSet {
            loadingView.stopAnimating()
            loadingView.animateOut()

            switch state {
            case .loading:
                inlineErrorView.animateOut()
            case .data(let listings):
                collectionView.refreshControl?.endRefreshing()
                inlineErrorView.animateOut()

                collectionView.animateIn()
                datasource.model = listings
                collectionView.reloadData()

            case .error(let model):
                collectionView.refreshControl?.endRefreshing()
                collectionView.animateOut()

                inlineErrorView.animateIn()
                inlineErrorView.errorModel = model
            }
        }
    }

    // MARK: - Overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "barbutton.categories".localized, style: .plain, target: self, action: #selector(showCategories(_:)))

        // Configure the collectionview
        collectionView.register(UINib(nibName: ListingResultCell.className, bundle: nil), forCellWithReuseIdentifier: ListingResultCell.className)
        collectionView.dataSource = datasource
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.groupTableViewBackground
        collectionView.refreshControl = UIRefreshControl(tintColor: .black, target: self, action: #selector(didPullToRefresh(_:)))
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = .defaultPadding
        layout.sectionInset = UIEdgeInsets(top: .defaultPadding, left: .defaultPadding, bottom: .defaultPadding, right: .defaultPadding)
        collectionView.collectionViewLayout = layout
        self.layout = layout

        // We setup our view with an initial loading state, a central loading view. Future loads will use the collectionView's refresh control
        collectionView.animateOut()
        loadingView.animateIn()
        loadingView.startAnimating()

        presenter?.start()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        // As our cells are screenwidth dependant, we need to resize!
        collectionView.collectionViewLayout.invalidateLayout()
    }

    // MARK: - Actions

    @objc
    func showCategories(_ sender: Any) {
        presenter?.showCategories()
    }

    @objc
    func didPullToRefresh(_ sender: Any) {
        presenter?.refresh()
    }
}

extension ListingResultViewController: ListingResultViewProtocol {

    func showError(_ errorModel: ErrorViewState) {
        present(errorModel.defaultAlertController, animated: true, completion: nil)
    }
}

extension ListingResultViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // We have a fixed size, but we rely on the bounds of the collectionview being up to date, so we can't just set the size directly on the layout
        return CGSize(width: collectionView.bounds.width - (layout.sectionInset.left + layout.sectionInset.right), height: .listingResultCellHeight)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard case let ListingResultViewState.Result.data(listings) = state else {
            preconditionFailure("Tapped a cell despite being in a non data state")

        }
        guard indexPath.row < listings.count else {
            preconditionFailure("Collectionview asked for model at index out of bounds")
        }
        presenter?.didTapItemWith(model: listings[indexPath.row])
    }
}

/// A single section model backed datasource, ensure the model is set before calling `UICollectionView.reloadData()`
internal final class ListingDatasource: NSObject {

    var model: [ListingResultViewState.Listing] = []
}

extension ListingDatasource: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard indexPath.row < model.count else {
            preconditionFailure("Collectionview asked for cell at index out of bounds")
        }

        if let resultCell = collectionView.dequeueReusableCell(withReuseIdentifier: ListingResultCell.className, for: indexPath) as? ListingResultCell {
            resultCell.setupWith(listing: model[indexPath.row])
            return resultCell
        }
        preconditionFailure("Tried to dequeue cell of unexpected cell class")
    }
}
