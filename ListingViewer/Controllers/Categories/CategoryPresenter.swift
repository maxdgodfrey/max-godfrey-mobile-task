//
//  CategoryPresenter.swift
//  ListingViewer
//
//  Created by Max on 18/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import ListingAPI
import PromiseKit

internal enum CategoryViewState {

    case loading
    case data([Category])
    case error(ErrorViewState)

    struct Category {
        let name: String
        let identifer: String
        let hasSubcategories: Bool
    }
}

internal protocol CategoryFlow: FlowDelegate {
    func showListings(_ categoryIdentifier: String, categoryName: String)
    func showCategoryWith(identifier: String)
    func dismissCategories()
    func back()
}

internal protocol CategoryPresenterProtocol: Presenter {

    func showListingsForCurrentCategory()
    func dimissCategories()
    func didSelectModel(_ model: CategoryViewState.Category)
}

internal final class CategoryPresenter {

    weak var view: CategoryViewProtocol?
    weak var flow: CategoryFlow?

    /// hold an in memory reference to our category
    var category: ListingAPI.Category?

    /// If our category presenter was built without a model, then we will use this to fetch our data
    var categoryIdentifier: String?

    let listingBroker: ListingBrokerProtocol // there is actually only one listing broker, All parties using it will hold a strong reference, but it is long lived.

    init(listingBroker: ListingBrokerProtocol, flow: CategoryFlow, category: ListingAPI.Category?) {
        self.category = category
        self.flow = flow
        self.listingBroker = listingBroker
    }

    init(listingBroker: ListingBroker, flow: CategoryFlow, identifier: String) {
        self.categoryIdentifier = identifier
        self.flow = flow
        self.listingBroker = listingBroker
    }
}

extension CategoryPresenter: CategoryPresenterProtocol {

    func start() {
        if let suppliedCategory = category {
            view?.title = suppliedCategory.displayCategoryName
            view?.state = viewStateFor(categories: suppliedCategory.subcategories)
            return
        }
    }

    func viewStateFor(categories: [ListingAPI.Category]?) -> CategoryViewState {
        guard let categories = categories, !categories.isEmpty else {
            let goBackAction: () -> Void = { [weak self] in
                self?.flow?.back()
            }

            return .error(ErrorViewState(title: "categoryList.noCategories.title".localized,
                                         message: "categoryList.noCategories.message".localized,
                                         actions: [("button.back".localized, goBackAction)]))
        }
        let viewCategories = categories.map { model in
            CategoryViewState.Category(name: model.name,
                                       identifer: model.number,
                                       hasSubcategories: !(model.isLeaf))
        }
        return .data(viewCategories)
    }

    private func fetchCategoryWith(identifier: String?) -> Promise<ListingAPI.Category> {
        if let suppliedIdentifier = identifier {
            return listingBroker.categoryWith(identifier: suppliedIdentifier)
        } else {
            return listingBroker.rootCategory()
        }
    }

    private func updateViewWith(category: ListingAPI.Category) {
        view?.title = category.displayCategoryName
        view?.state = viewStateFor(categories:  category.subcategories)
    }

    func showListingsForCurrentCategory() {
        firstly {
            fetchCategoryWith(identifier: categoryIdentifier)
        }.done { fetchedCategory in
            self.category = fetchedCategory
            self.flow?.showListings(fetchedCategory.number, categoryName: fetchedCategory.displayCategoryName)
            self.updateViewWith(category: fetchedCategory)
        }.catch { _ in
            // notify the view that it should move to an error state
            self.view?.state = .error(.defaultInline())
        }
    }

    func showListingsWith(identifier: String, categoryName: String) {
        flow?.showListings(identifier, categoryName: categoryName)
    }

    func dimissCategories() {
        flow?.dismissCategories()
    }

    func didSelectModel(_ model: CategoryViewState.Category) {
        if model.hasSubcategories {
            self.flow?.showCategoryWith(identifier: model.identifer)
        } else {
            showListingsWith(identifier: model.identifer, categoryName: model.name)
        }
    }
}

extension ListingAPI.Category {

    var displayCategoryName: String {
        // Workaround for when we navigate back to the root of the category tree.
        if name == "Root" {
            return "categoryList.root.title".localized
        }
        return name
    }
}
