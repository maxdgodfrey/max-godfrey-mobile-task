//
//  CategoryViewController.swift
//  ListingViewer
//
//  Created by Max on 18/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit

internal protocol CategoryViewProtocol: MVPView {
    var title: String? { get set }
    var state: CategoryViewState { get set }
}

internal final class CategoryViewController: UIViewController {

    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var errorView: InlineErrorView!

    var previousIndexPath: IndexPath?
    var datasource: SingleSectionModelBackedDatasouce?
    var presenter: CategoryPresenterProtocol?
    var models: [CategoryViewState.Category] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    var state: CategoryViewState = .loading {
        didSet {
            resetViewState()
            switch state {
            case .data(let categories):
                tableView.isHidden = false
                showData(categories)
            case .error(let error):
                errorView.isHidden = false
                errorView.errorModel = error
            case .loading:
                loadingView.isHidden = false
                showLoading()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        datasource = SingleSectionModelBackedDatasouce()
        tableView.dataSource = datasource
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.className)

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissCategories(_:)))

        resetViewState()
        loadingView.isHidden = false
        showLoading()
        presenter?.start()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.showListingsForCurrentCategory()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        /*
         Clean up the tableview's state after we dissapear, this ensures the cell's selection state is treated correctly (that is selected cells stay selected until the user
         vists another subcategory)
         */
        previousIndexPath = nil
        tableView.reloadData()
    }

    @objc
    func dismissCategories(_ sender: UIBarButtonItem) {
        presenter?.dimissCategories()
    }
}

extension CategoryViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row < models.count, let cell = tableView.cellForRow(at: indexPath) else {
            preconditionFailure("Tableview asked for indexpath without a corresponding model")
        }
        if let previousIndexPath = previousIndexPath {
            // An awkward workaround to acheive single row disclosure tick.
            tableView.deselectRow(at: previousIndexPath, animated: false)
            tableView.reloadRows(at: [previousIndexPath], with: .fade)
        }
        previousIndexPath = indexPath
        let model = models[indexPath.row]
        model.populate(cell: cell)
        presenter?.didSelectModel(model)
    }
}

extension CategoryViewState.Category: TableViewRenderable {

    var reuseIdentifer: String {
        return UITableViewCell.className
    }

    func populate(cell: UITableViewCell) {
        cell.textLabel?.text = name
        if hasSubcategories {
            cell.accessoryType = .disclosureIndicator
            cell.selectionStyle = .default
        } else {
            cell.accessoryType = cell.isSelected ? .checkmark : .none
            cell.selectionStyle = .none
        }
    }
}

extension CategoryViewController: CategoryViewProtocol {

    func resetViewState() {
        loadingView.stopAnimating()
        loadingView.isHidden = true
        tableView.isHidden = true
        errorView.isHidden = true
    }

    func showData(_ categories: [CategoryViewState.Category]) {
        datasource?.models = categories
        self.models = categories
    }

    func showLoading() {
        loadingView.startAnimating()
    }
}

internal protocol TableViewRenderable {

    var reuseIdentifer: String { get }
    func populate(cell: UITableViewCell)
}

final internal class SingleSectionModelBackedDatasouce: NSObject, UITableViewDataSource {

    var models: [TableViewRenderable] = []

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < models.count else {
            preconditionFailure("Tableview asked for indexpath without a corresponding model")
        }
        let model = models[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: model.reuseIdentifer, for: indexPath)
        model.populate(cell: cell)
        return cell
    }
}
