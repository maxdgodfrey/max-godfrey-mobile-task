//
//  CategoryTree.swift
//  ListingViewer
//
//  Created by Max on 19/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import ListingAPI

struct CategoryNode {
    let name: String
    let identifier: String
}

indirect enum CategoryTree {
    case leaf(CategoryNode)
    case children(CategoryNode, [CategoryTree])
}

extension CategoryTree {

    init(category: ListingAPI.Category) {
        // first make a node for me
        let node = CategoryNode(category: category)
        if let subcategories = category.subcategories, !subcategories.isEmpty {
            self = .children(node, subcategories.map(CategoryTree.init))
        } else { // we are a leaf
            self = .leaf(node)
        }
    }
}

extension CategoryNode {

    init(category: ListingAPI.Category) {
        self.name = category.name
        self.identifier = category.number
    }
}
