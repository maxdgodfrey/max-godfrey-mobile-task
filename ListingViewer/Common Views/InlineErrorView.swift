//
//  InlineErrorView.swift
//  ListingViewer
//
//  Created by Max on 19/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit

internal final class InlineErrorView: BaseNibLoadableView {

    @IBOutlet weak var containerStackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var reasonLabel: UILabel!

    var actions: [() -> Void] = []
    var buttons: [UIButton] = []

    var errorModel: ErrorViewState? {
        didSet {
            // reset our state
            actions = []
            buttons.forEach { $0.removeFromSuperview() }
            buttons = []

            titleLabel.text = errorModel?.title
            messageLabel.text = errorModel?.message
            reasonLabel.text = errorModel?.reason
            reasonLabel.textColor = UIColor.gray
            actions = (errorModel?.actions.map { $0.1 }) ?? []
            buttons = errorModel?.actions.map { (title, _) in
                let button = UIButton(type: UIButtonType.system)
                button.translatesAutoresizingMaskIntoConstraints = false
                button.addTarget(self, action: #selector(buttonTapped(sender:)), for: .touchUpInside)
                button.setTitle(title, for: .normal)
                return button
            } ?? []
            buttons.forEach { self.containerStackView.addArrangedSubview($0) }
        }
    }

    @objc
    func buttonTapped(sender: UIButton) {
        guard let actionIndex = buttons.index(of: sender), actionIndex < actions.count else {
            assertionFailure("Tried to call action of button that didn't have corresponding index.")
            return
        }
        actions[actionIndex]()
    }

    override func prepare() {
        super.prepare()
        backgroundColor = UIColor.groupTableViewBackground
    }
}
