//
//  NibLoadableView.swift
//  ListingViewer
//
//  Created by Max on 19/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit

internal protocol NibLoadableView {
    func fromNib(bundle: Bundle) -> UIView?
}

extension NibLoadableView where Self: UIView {

    /// Instansiate a UIView subclass from a nib with a name matching the class name.
    ///
    /// - Parameter bundle: The bundle in which to find the .xib
    func fromNib(bundle: Bundle = Bundle.main) -> UIView? {
        return bundle.loadNibNamed(className, owner: self, options: nil)?[0] as? UIView
    }
}

internal class BaseNibLoadableView: UIView, NibLoadableView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup(view: fromNib())
        prepare()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup(view: fromNib())
        prepare()
    }

    func prepare() {
        // subclass hook to do one time view setup.
    }

    private func setup(view: UIView?) {
        guard let view = view else {
            // Programmer error, possibly a typo in the nib name.
            assertionFailure("Failed to load view from Nib")
            return
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(equalTo: leadingAnchor),
            view.trailingAnchor.constraint(equalTo: trailingAnchor),
            view.topAnchor.constraint(equalTo: topAnchor),
            view.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
