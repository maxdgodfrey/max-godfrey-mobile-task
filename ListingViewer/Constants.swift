//
//  Constants.swift
//  ListingViewer
//
//  Created by Max on 18/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit

extension TimeInterval {

    static let splashScreenAnimationDuration: TimeInterval = 0.5
    static let splashScreenAnimationDelay: TimeInterval = 0.5
    static let splashScreenAnimationFadeOutDuration: TimeInterval = 0.2

    static let defaultAnimationFadeDuration: TimeInterval = 0.2
}

extension CGFloat {

    static let splashScreenDampingRatio: CGFloat = 0.9
    static let defaultPadding: CGFloat = 10
    static let defaultCornerRadius: CGFloat = 4
    static let listingResultCellHeight: CGFloat = 96
    static let refreshControlWidth: CGFloat = 40
    static let refreshControlHeight: CGFloat = 40
}

extension CGSize {

    static let refreshControlSize: CGSize = CGSize(width: .refreshControlWidth, height: .refreshControlHeight)
}
