# Readme #

## Contents ##
* How to run 
    - Sandbox
    - Mock
    - Enabling swiftlint
* Architecture breakdown
    - MVP Approach
* Future work
    - Tests
    - Persistence
    - Documentation

### Getting Started ###

I am using CocoaPods, so ensure you open the `ListingViewer.xcworkspace`. This was built using Xcode Version 9.2 and targets iOS 11.0 and above. **The app is currently setup to build with my personal team, so that will need to be changed to build to a device.** 

There are two schemes of relevance:
- `ListingViewer - Sandbox`, this points to the sandbox environment. **This is the one to run to see my work**.
- `ListingViewer - Mock`, this doesn't leverage any real APIs and returns canned responses, there are two second delays in place to excersize the loading views. Also the images returned are images of the last part of the url we are trying to load - useful for debugging. 
There are other schemes (all the pods targets for instance) and for the API framework (`ListingAPI`) but you should not need to build these directly.

#### Optional ####
I have disabled Swiftlint due to issues around it ignoring the rules file (`.swiftlint.yml`) on older versions. But if you're running the latest then you can re add the Swiftlint build phase that runs it and you'll be good to go.

### Architecture Breakdown ###

The App was built with a Model View Presenter approach, with the goal of pulling business rules out of the View layer and into a "Presenter" that can be tested. In practice I didn't have that much time to test, but it makes the control flow easier for me to reason about. Also the view can almost be inferred from the state that the presenter updates. For more detail read on:

#### MVP ####

For most screens there is a "ViewState" type that captures the valid states for a view to be in. Typically this is loading, content (i.e. something to render) or and error. 

Once the ViewController has loaded, it hands off to it's Presenter (via the `start()` method). Now the Presenter is in control, updating the view's state as it loads data, handles errors etc. The ViewController and Presenter are both behind protocols to allow them to be mocked when testing. The Presenter is responsibile for responding to user actions from, and pushing state changes back into the View. Under the hood, the Presenter is also given a _Flow_ (see below) and often a _Broker_ to fetch data (from the network of local storage). 

**The Flow**

A flow is a take on the Coordinator pattern, where the logic of moving between screens and how to actually do it, is lifted into a new construct called a _Coordinator_. This app, as it has a pretty simple flow of navigation, has one coordinator - the `AppCoordinator`. This is responsible for moving between screens, and handing data between them. An obvious alternative to this is using Storyboards and segues. However this means a given screen explicitly knows of it's neighbours, and what data they need. 
While the with the coordinator pattern it is harder to visualise the "app navigation" compared to a Storyboard, it lets us control the view controller creation, which we need to be able to inject presenters etc. 

You'll notice that each presenter has it's own "FlowProtocol". This represents exactly where this presenter can go. For instance the listing detail screen can only go back, it can't show the category picker like the listing result screen. 

**MVP Factory**

This is a class to construct, assemble the view controllers and create presenters with their dependencies (injecting brokers etc.). The code is quite "boilerplate-ee" but is awkward to abstract futher. I can talk to this IRL if given an interview.

### Future Work ###

#### Tests ####

I ran out of time to properly unit test all the presenters and the decoding logic. I have roughed out a SplashScreenPresenter test to show how you can test the flows, but it's not as atomic or tidy as I would like. 

There are unit tests for decoding the Category model, but again ideally all models would have decoding tests.

#### Core Data ####

The app hits the backend quite frequently, relying on the default caching behaviour of the default session. Ideally, the app could fetch a sensible depth of the category tree and persist (or at least cache it) as I don't imagine the categories change often. This could be refreshed on app launch during the initial "Splash screen load".

If I were to implement this, the logic would live or at least be kicked off in the Broker. For instance, the `ListingBroker` is very much a passthrough at the moment, that's because I ran out of time to tackle this. 

#### Documentation ####

While some protocols and extensions are documented, there is a fair portion that are not. I prioritized the tests over the documentation but ran out of time to really nail both. 