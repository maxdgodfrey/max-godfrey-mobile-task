//
//  Category.swift
//  ListingAPI
//
//  Created by Max on 17/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation

public struct Category {

    public let name: String
    public let number: String // A unique identifier for the category e.g. “0004-0369-6076-“. We plan to change this to a numeric identifier (e.g. “6076”) so you should ensure you can cope with both formats.
    public let subcategories: [Category]?
    public let isLeaf: Bool
}

/// Conformance in extension as we want the default struct initializer
extension Category: Decodable {

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case number = "Number"
        case subcategories = "Subcategories"
        case isLeaf = "IsLeaf"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        // the decoder should coerce 6076 to "6076"
        let numberIdentifier = try container.decode(String.self, forKey: .number)
        let name = try container.decode(String.self, forKey: .name)
        let subcategories = try container.decodeIfPresent([Category].self, forKey: .subcategories)
        let isLeaf = try container.decode(Bool.self, forKey: .isLeaf)

        self.init(name: name,
                  number: numberIdentifier,
                  subcategories: subcategories,
                  isLeaf: isLeaf)
    }
}

extension Category: Equatable {

    public static func == (lhs: Category, rhs: Category) -> Bool {
        return lhs.name == rhs.name &&
            lhs.number == rhs.number &&
            lhs.isLeaf == rhs.isLeaf
    }
}
