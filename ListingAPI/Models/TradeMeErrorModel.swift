//
//  TradeMeErrorModel.swift
//  ListingAPI
//
//  Created by Max on 20/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation

/// Temporary DTO for decoding a TradeMe error JSON object, leveraging default generated `init(from:)`
internal struct TradeMeError: Decodable {
    let errorDescription: String

    enum CodingKeys: String, CodingKey {
        case errorDescription = "ErrorDescription"
    }
}
