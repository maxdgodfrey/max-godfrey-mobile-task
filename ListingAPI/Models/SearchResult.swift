//
//  SearchResult.swift
//  ListingAPI
//
//  Created by Max on 18/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation

public struct SearchResult {

    public let listings: [Listing]
}

extension SearchResult: Decodable {

    enum CodingKeys: String, CodingKey {
        case listings = "List"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let listings = try container.decode([Listing].self, forKey: .listings)
        self.init(listings: listings)
    }
}

public struct Listing {

    public let identifier: Int
    public let title: String
    public let subtitle: String?
    public let priceDisplay: String?
    public let imageHref: String?
}

extension Listing: Decodable {

    enum CodingKeys: String, CodingKey {
        case identifier = "ListingId"
        case title = "Title"
        case subtitle = "Subtitle"
        case priceDisplay = "PriceDisplay"
        case imageHref = "PictureHref"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let identifier = try container.decode(Int.self, forKey: .identifier)
        let title = try container.decode(String.self, forKey: .title)
        let subtitle = try container.decodeIfPresent(String.self, forKey: .subtitle)
        let priceDisplay = try container.decodeIfPresent(String.self, forKey: .priceDisplay)
        let imageHref = try container.decodeIfPresent(String.self, forKey: .imageHref)
        self.init(identifier: identifier,
                  title: title,
                  subtitle: subtitle,
                  priceDisplay: priceDisplay,
                  imageHref: imageHref)
    }
}
