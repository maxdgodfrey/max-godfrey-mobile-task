//
//  ListingDetail.swift
//  ListingAPI
//
//  Created by Max on 19/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation

public struct ListingDetail {

    public let title: String
    public let subtitle: String?
    public let category: String

    public let region: String?
    public let suburb: String?
    public let priceDisplay: String?

//    public let startDate: Date
//    public let endDate: Date

    public let photos: [ListingDetailPhoto]?
}

extension ListingDetail: Decodable {

    enum CodingKeys: String, CodingKey {
        case title = "Title"
        case subtitle = "Subtitle"
        case category = "CategoryName"
        case region = "Region"
        case suburb = "Suburb"
        case priceDisplay = "PriceDisplay"
//        case startDate = "StartDate"
//        case endDate = "EndDate"
        case photos = "Photos"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let photosAttributeArray = try container.decodeIfPresent([ListingDetailAttributeResolver<ListingDetailPhoto>].self, forKey: .photos)
        self.init(title: try container.decode(String.self, forKey: .title),
                  subtitle: try container.decodeIfPresent(String.self, forKey: .subtitle),
                  category: try container.decode(String.self, forKey: .category),
                  region: try container.decodeIfPresent(String.self, forKey: .region),
                  suburb: try container.decodeIfPresent(String.self, forKey: .suburb),
                  priceDisplay: try container.decodeIfPresent(String.self, forKey: .priceDisplay),
                  photos: photosAttributeArray?.map { $0.value })
    }
}

public struct ListingDetailAttributeResolver<A: Decodable> {
    let key: Int
    let value: A
}

extension ListingDetailAttributeResolver: Decodable {

    enum CodingKeys: String, CodingKey {
        case key = "Key"
        case value = "Value"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.init(key: try container.decode(Int.self, forKey: .key),
                  value: try container.decode(A.self, forKey: .value))
    }
}

public struct ListingDetailPhoto {
    public let medium: URL
    public let large: URL
}

extension ListingDetailPhoto: Decodable {

    enum CodingKeys: String, CodingKey {
        case medium = "Medium"
        case large = "Large"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.init(medium: try container.decode(URL.self, forKey: .medium),
                  large: try container.decode(URL.self, forKey: .large))
    }
}
