//
//  BrokerFactory.swift
//  ListingViewer
//
//  Created by Max on 16/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation
import AlamofireImage

public struct ListingAPIDecoding {

    public static let defaultDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        return decoder
    }()
}

public final class BrokerFactory {

    private let webservice = Webservice(session: URLSession.shared, base: BuildConfig.shared.baseURL, errorAdapter: AuthenticatedErrorAdapter())
    private let imageService = ImageDownloader()

    public let listingBroker: ListingBroker

    public init() {
        #if MOCK
            let categoryService = MockCategoryService()
        #else
            let categoryService = ListingService(webservice: webservice, imageService: imageService)
        #endif
        self.listingBroker = ListingBroker(service: categoryService)
    }
}
