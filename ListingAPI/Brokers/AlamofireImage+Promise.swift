//
//  PromiseConformance.swift
//  ListingAPI
//
//  Created by Max on 19/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//
import Alamofire
import AlamofireImage
import PromiseKit

extension ImageDownloader {

    open func download(
        _ urlRequest: Alamofire.URLRequestConvertible,
        receiptID: String = UUID().uuidString,
        filter: ImageFilter? = nil,
        progress: ProgressHandler? = nil,
        progressQueue: DispatchQueue = DispatchQueue.main) -> Promise<Image> {
        return Promise { seal in
            download(urlRequest, receiptID: receiptID, filter: filter, progress: progress, progressQueue: progressQueue) { response in
                if let image = response.result.value {
                    seal.fulfill(image)
                } else if let error = response.error {
                    seal.reject(error)
                } else {
                    seal.reject(TMError.unknown)
                }
            }
        }
    }
}
