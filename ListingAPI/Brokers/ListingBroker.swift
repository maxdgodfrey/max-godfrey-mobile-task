//
//  ListingBroker.swift
//  ListingAPI
//
//  Created by Max on 17/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation
import PromiseKit

public protocol ListingBrokerProtocol {

    func rootCategory() -> Promise<Category>
    func categoryWith(identifier: String) -> Promise<Category>
    func listingsForCategory(_ categoryId: String) -> Promise<SearchResult>
    func listingResultImage(for url: URL) -> Promise<UIImage>
    func listingDetailFor(identifier: Int) -> Promise<ListingDetail>
}

/// The listing broker ended up being pretty light, but this could have been a place to add caching or core data wrappers.
public final class ListingBroker {

    let service: ListingServiceProtocol
    
    init(service: ListingServiceProtocol) {
        self.service = service
    }

}

extension ListingBroker: ListingBrokerProtocol {
    
    public func rootCategory() -> Promise<Category> {
        return service.getRootCategories()
    }

    public func categoryWith(identifier: String) -> Promise<Category> {
        return service.getCategoryWith(identifier: identifier)
    }

    public func listingsForCategory(_ categoryId: String) -> Promise<SearchResult> {
        return service.getSearchResults(for: categoryId)
    }

    public func listingResultImage(for url: URL) -> Promise<UIImage> {
        return service.getImage(url: url)
    }

    public func listingDetailFor(identifier: Int) -> Promise<ListingDetail> {
        return service.getListingDetailWith(identifier: identifier)
    }
}
