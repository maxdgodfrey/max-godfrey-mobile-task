//
//  ImageExtensions.swift
//  ListingViewer
//
//  Created by Max on 19/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import UIKit

public extension UIImage {

    /// Retrieved from 19/03/18 - https://stackoverflow.com/questions/26542035/create-uiimage-with-solid-color-in-swift
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }

    /// Retrieved from as at 20/03/18 - https://stackoverflow.com/questions/11867152/how-to-create-an-image-from-uilabel
    static func imageWith(view: UIView) -> UIImage {
        // swiftlint:disable force_unwrapping
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
        // swiftlint:enable force_unwrapping
    }
}
