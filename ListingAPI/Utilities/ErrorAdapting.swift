//
//  ErrorAdapting.swift
//  ListingAPI
//
//  Created by Max on 20/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation

internal protocol ErrorAdapter {
    func errorFromReponse(_ response: HTTPURLResponse, data: Data?) -> Error?
}

public enum TMError: Error {
    case unknown
    case serverError(description: String?)
    case unauthorized // I am really hoping I don't run into this.
}

internal final class AuthenticatedErrorAdapter: ErrorAdapter {

    func errorFromReponse(_ response: HTTPURLResponse, data: Data?) -> Error? {
        let statusCode = response.statusCode
        guard 200 < statusCode && statusCode > 300 else {
            return nil // no error
        }

        switch statusCode {
        case 401, 403:
            return TMError.unauthorized
        case 500:
            if let data = data {
                let description = try? ListingAPIDecoding.defaultDecoder.decode(TradeMeError.self, from: data).errorDescription
                return TMError.serverError(description: description)
            }
            return TMError.serverError(description: nil)
        case 501..<599, 404:
            return TMError.unknown
        default:
            return nil
        }
    }
}
