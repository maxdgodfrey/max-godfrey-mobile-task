//
//  BuildConfig.swift
//  ListingAPI
//
//  Created by Max on 16/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation

public class BuildConfig {

    public static let shared: BuildConfig = BuildConfig()

    private lazy var plist: BuildConfigPlist = {
        BuildConfigPlist.loadConfigurationSettings()
    }()

    public lazy var baseURL: URL = {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = plist.apiBaseURL
        urlComponents.path = plist.apiVersion
        // A build time error, build building the app and running it once per config this is proven to work.
        // swiftlint:disable force_unwrapping
        return urlComponents.url!
        // swiftlint:enable force_unwrapping
    }()
}

/// Model for reading out build configuration related settings
internal struct BuildConfigPlist: Decodable {

    let apiBaseURL: String
    let apiVersion: String

    private enum CodingKeys: String, CodingKey {
        case apiBaseURL = "API_BASE_URL"
        case apiVersion = "API_VERSION"
    }

    static func loadConfigurationSettings(bundle: Bundle = Bundle.main) -> BuildConfigPlist {
        guard let plistPath = bundle.url(forResource: "configuration", withExtension: "plist") else {
            preconditionFailure("unable to locate the configuration.plist file on startup, These are copied into the bundle as a build phase. Check in there!")
        }
        do {
            let plistData = try Data(contentsOf: plistPath)
            let config = try PropertyListDecoder().decode(BuildConfigPlist.self, from: plistData)
            return config
        } catch {
            preconditionFailure("Error reading plist: \(error)")
        }
    }

}
