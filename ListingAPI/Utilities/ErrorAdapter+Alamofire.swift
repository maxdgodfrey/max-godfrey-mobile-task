//
//  ErrorAdapter+Alamofire.swift
//  ListingAPI
//
//  Created by Max on 20/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Alamofire

extension ErrorAdapter {

    var validation: Alamofire.DataRequest.Validation {
        return { request, response, data in
            guard let error = self.errorFromReponse(response, data: data) else {
                return Request.ValidationResult.success
            }
            return Request.ValidationResult.failure(error)
        }
    }
}
