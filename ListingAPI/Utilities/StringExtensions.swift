//
//  StringExtensions.swift
//  ListingAPI
//
//  Created by Max on 17/03/18.
//  Copyright © 2018 Max Godfrey. All rights reserved.
//

import Foundation

extension String {

    var utf8Data: Data {
        // swiftlint:disable force_unwrapping
        return self.data(using: .utf8)!
        // swiftlint:enable force_unwrapping
    }
}
